source 'https://rubygems.org'
ruby '2.1.2'
gem 'rails', '~> 4.2.0'
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0',          group: :doc
gem 'spring',        group: :development
gem 'devise', '~> 3.4.1'
gem "figaro"
gem 'foundation-rails'
gem 'haml-rails'
gem 'puma'
gem 'sidekiq'
gem "geocoder"
gem 'impressionist'
gem 'gaffe'
gem 'haversine', '~> 0.3.0'
gem 'jquery-datatables-rails', '~> 1.12.2'
gem 'ajax-datatables-rails'
gem 'acts-as-taggable-on'
gem 'rails3-jquery-autocomplete'
gem 'will_paginate', '~> 3.0'
gem 'will_paginate-bootstrap'
gem 'bcrypt', '~> 3.1.7'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'twitter'
gem 'open_uri_redirections'
gem 'bootsy'
gem 'social-share-button', '~> 0.1.6'
gem 'aes', '~> 0.5.0'
gem 'certified', '~> 1.0.0'
gem 'recaptcha', '~> 0.4.0'
gem "paperclip", "~> 4.1.0"
gem 'papercrop', '~> 0.2.0'
gem 'aws-sdk', '~> 1.5.7'
gem 'aws-ses', '~> 0.4.4', require: 'aws/ses'
gem "actionpack-action_caching", github: "rails/actionpack-action_caching"
gem 'google-analytics-rails'
gem 'jwt'

group :production do
  gem 'passenger'
  gem 'pg', '0.16.0'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller', :platforms=>[:mri_21]
  gem 'guard-bundler'
  gem 'guard-rails'
  gem 'guard-rspec'
  gem 'html2haml'
  gem 'quiet_assets'
  gem 'rails_layout'
  gem 'rb-fchange', :require=>false
  gem 'rb-fsevent', :require=>false
  gem 'rb-inotify', :require=>false
  gem 'spring-commands-rspec'
end

group :development, :test do
  gem 'factory_girl_rails'
  gem 'pry-rails'
  gem 'pry-rescue'
  gem 'faker', '~> 1.4.3'
  gem 'sqlite3'
  gem 'rails_apps_testing'
  gem 'rspec-rails', '~> 3.0'
  gem 'launchy'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
end

group :test do
  gem 'capybara'
  gem 'capybara-webkit'
  gem 'database_cleaner'
end
