# AbilityList.org

This is the repository for the [AbilityList website](https://www.abilitylist.org).

This is a [Rails](http://rubyonrails.org/) project, which emphasizes well-known software engineering patterns and paradigms, including [convention over configuration (CoC)](http://en.wikipedia.org/wiki/Convention_over_configuration), [don't repeat yourself (DRY)](http://en.wikipedia.org/wiki/Don%27t_repeat_yourself), the [active record pattern](http://en.wikipedia.org/wiki/Active_record_pattern), and [model–view–controller (MVC)](http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller). The app is hosted on [Amazon EC2](http://aws.amazon.com/ec2/)
and stores static resources like images on [S3](http://aws.amazon.com/s3/)/ [Cloudfront](http://aws.amazon.com/cloudfront/).

## Contributions Welcome!

If you find a typo or you feel like you can improve the HTML, CSS, or
JavaScript, we welcome contributions. Feel free to open issues or pull
requests like any normal GitHub project, and we'll merge it in.

## Running the Site Locally

Running the site locally is simple. Clone this repo and run the following
commands:

```
$ bundle install
$ rake db:migrate
$ rake db:seed                # this will add a few users and resources
$ rails console               # the seed routine doesn't trigger callbacks perfectly
  > Resource.all.map { |m| m.clean_tags_string and m.save } # trigger this funtion manually 
  > exit
$ rails s
```
Then open up `localhost:3000`.

## Testing

Like most Rails apps, AbilityList has a test suite. The full suite can be run with:

```
 $ rspec
```
  
