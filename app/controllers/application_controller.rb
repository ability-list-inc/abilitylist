class ApplicationController < ActionController::Base
	before_action :configure_devise_permitted_parameters, if: :devise_controller?
	before_action :fetchSignUpModalCookie
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

	def fetchSignUpModalCookie
		if cookies[:abilityListSignUpModalCookie]
			@sign_up_modal_cookie_present = true
		else
			@sign_up_modal_cookie_present = false
			cookies[:abilityListSignUpModalCookie] = { value: "true", expires: 1.hour.from_now }
		end
	end

  def after_sign_in_path_for(resource)
		cookies[:abilityListSignUpModalCookie] = { value: "true", expires: 1.year.from_now }
  	user_path(current_user) #your path
	end

	def configure_devise_permitted_parameters
    registration_params = [:first_name, :last_name, :email, :dob, :address_street, :address_city, :address_state, :address_city, :address_postal, :phone, :password, :password_confirmation]

		if params[:action] == 'create'
      devise_parameter_sanitizer.for(:sign_up) {
        |u| u.permit(registration_params)
      }
    end

  end

end
