class ConversationsController < ApplicationController
  before_action :set_conversation, only: [:show, :edit, :update, :destroy]

  # GET /conversations
  # GET /conversations.json
  def index
    @conversations = Conversation.all
  end

  # GET /conversations/1
  # GET /conversations/1.json
  def show
  end

  # GET /conversations/new
  def new
    @conversation = Conversation.new
  end

  # GET /conversations/1/edit
  def edit
  end

  # POST /conversations
  # POST /conversations.json
  def create

    # first, user must be signed in
    # second, must not already have conversation with other participant
    conversation_id = nil # default
    if user_signed_in?

      sql = "SELECT conversation_id FROM conversations_users WHERE user_id = " + current_user.id.to_s + ";"
      rawResults = ActiveRecord::Base.connection.execute(sql)

      conversationIdsOfCurrentUser = Array.new
      rawResults.each do |rr|
        conversationIdsOfCurrentUser.push(rr["conversation_id"])
      end

      conversationsWithOtherParticipant = ConversationsUser.where(
        user_id: params[:other_participant_id],
        conversation_id: conversationIdsOfCurrentUser.split(",")
      )
      if conversationsWithOtherParticipant.length == 1
        conversation_id = conversationsWithOtherParticipant[0]["conversation_id"]
      end
    else
      # not signed in
      flash[:notice] = 24
      redirect_to "/users/sign_in" and return
    end
    if conversation_id == nil
      @conversation = Conversation.new(Hash.new)
      conversationCreated = @conversation.save
      firstLinkCreated = ConversationsUser.create(
        :user_id => current_user.id,
        :conversation_id => @conversation.id
      )
      secondLinkCreated = ConversationsUser.create(
        :user_id => params[:other_participant_id],
        :conversation_id => @conversation.id
      )

      respond_to do |format|
        if conversationCreated and firstLinkCreated and secondLinkCreated
          redirect_location = "/users/#{current_user.id}/conversations/#{@conversation.id}"
          format.html { redirect_to redirect_location }
          format.json { render :show, status: :created, location: @conversation }
        else
          format.html { render :new }
          format.json { render json: @conversation.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to "/users/#{current_user.id}/conversations/#{conversation_id}"
    end
  end

  # PATCH/PUT /conversations/1
  # PATCH/PUT /conversations/1.json
  def update
    respond_to do |format|
      if @conversation.update(conversation_params)
        format.html { redirect_to @conversation, notice: 'Conversation was successfully updated.' }
        format.json { render :show, status: :ok, location: @conversation }
      else
        format.html { render :edit }
        format.json { render json: @conversation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conversations/1
  # DELETE /conversations/1.json
  def destroy
    @conversation.destroy
    respond_to do |format|
      format.html { redirect_to conversations_url, notice: 'Conversation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conversation
      @conversation = Conversation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conversation_params
      params.require(:conversation).permit(:last_message_sent_at, :access_token)
    end
end
