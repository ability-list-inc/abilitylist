class ConversationsUsersController < ApplicationController
  before_action :set_conversations_user, only: [:show, :edit, :update, :destroy]

  # GET /conversations_users
  # GET /conversations_users.json
  def index
    @conversations_users = ConversationsUser.all
  end

  # GET /conversations_users/1
  # GET /conversations_users/1.json
  def show
  end

  # GET /conversations_users/new
  def new
    @conversations_user = ConversationsUser.new
  end

  # GET /conversations_users/1/edit
  def edit
  end

  # POST /conversations_users
  # POST /conversations_users.json
  def create
    @conversations_user = ConversationsUser.new(conversations_user_params)

    respond_to do |format|
      if @conversations_user.save
        format.html { redirect_to @conversations_user, notice: 'Conversations user was successfully created.' }
        format.json { render :show, status: :created, location: @conversations_user }
      else
        format.html { render :new }
        format.json { render json: @conversations_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /conversations_users/1
  # PATCH/PUT /conversations_users/1.json
  def update
    respond_to do |format|
      if @conversations_user.update(conversations_user_params)
        format.html { redirect_to @conversations_user, notice: 'Conversations user was successfully updated.' }
        format.json { render :show, status: :ok, location: @conversations_user }
      else
        format.html { render :edit }
        format.json { render json: @conversations_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conversations_users/1
  # DELETE /conversations_users/1.json
  def destroy
    @conversations_user.destroy
    respond_to do |format|
      format.html { redirect_to conversations_users_url, notice: 'Conversations user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conversations_user
      @conversations_user = ConversationsUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conversations_user_params
      params.require(:conversations_user).permit(:user_id, :conversation_id, :joined_at)
    end
end
