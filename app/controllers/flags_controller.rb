class FlagsController < ApplicationController
  before_action :set_flag, only: [:show, :edit, :update, :destroy]

  # GET /flags
  # GET /flags.json
  def index
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @flags = Flag.all
    end
  end

  # GET /flags/1
  # GET /flags/1.json
  def show
  end

  # GET /flags/new
  def new
    @flag = Flag.new
  end

  # GET /flags/1/edit
  def edit
  end

  # POST /flags
  # POST /flags.json
  def create
    @flag = Flag.new(flag_params)

    if current_user.can_favorite_resource(@flag.user_id)
      respond_to do |format|
        if @flag.save
          format.html { redirect_to "/resources/" + @flag.resource_id.to_s, notice: 'Flag was successfully created.' }
          format.json { render :show, status: :created, location: @flag }
        else
          format.html { redirect_to "/resources/" + @flag.resource_id.to_s }
          format.json { render json: @flag.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /flags/1
  # PATCH/PUT /flags/1.json
  def update
    if current_user.can_favorite_resource(@flag.user_id)    
      respond_to do |format|
        if @flag.update(flag_params)
          format.html { redirect_to "/resources/" + @flag.resource_id.to_s, notice: 'Flag was successfully updated.' }
          format.json { render :show, status: :ok, location: @flag }
        else
          format.html { redirect_to "/resources/" + @flag.resource_id.to_s }
          format.json { render json: @flag.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /flags/1
  # DELETE /flags/1.json
  def destroy
    if current_user.can_favorite_resource(@flag.user_id)
      resource_id = @flag.resource_id
      @flag.destroy
      respond_to do |format|
        format.html { redirect_to "/resources/" + resource_id.to_s, notice: 'Flag was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flag
      @flag = Flag.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def flag_params
      params.require(:flag).permit(:category, :resource_id, :user_id, :duplicate_id, :details, :bootsy_image_gallery_id)
    end
end
