class GeocodedAddressesController < ApplicationController
  before_action :set_geocoded_address, only: [:show, :edit, :update, :destroy]

  # GET /geocoded_addresses
  # GET /geocoded_addresses.json
  def index
    @geocoded_addresses = GeocodedAddress.all
  end

  # GET /geocoded_addresses/1
  # GET /geocoded_addresses/1.json
  def show
  end

  # GET /geocoded_addresses/new
  def new
    @geocoded_address = GeocodedAddress.new
  end

  # GET /geocoded_addresses/1/edit
  def edit
  end

  # POST /geocoded_addresses
  # POST /geocoded_addresses.json
  def create
    @geocoded_address = GeocodedAddress.new(geocoded_address_params)

    respond_to do |format|
      if @geocoded_address.save
        format.html { redirect_to @geocoded_address, notice: 'Geocoded address was successfully created.' }
        format.json { render :show, status: :created, location: @geocoded_address }
      else
        format.html { render :new }
        format.json { render json: @geocoded_address.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /geocoded_addresses/1
  # PATCH/PUT /geocoded_addresses/1.json
  def update
    respond_to do |format|
      if @geocoded_address.update(geocoded_address_params)
        format.html { redirect_to @geocoded_address, notice: 'Geocoded address was successfully updated.' }
        format.json { render :show, status: :ok, location: @geocoded_address }
      else
        format.html { render :edit }
        format.json { render json: @geocoded_address.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /geocoded_addresses/1
  # DELETE /geocoded_addresses/1.json
  def destroy
    @geocoded_address.destroy
    respond_to do |format|
      format.html { redirect_to geocoded_addresses_url, notice: 'Geocoded address was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_geocoded_address
      @geocoded_address = GeocodedAddress.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def geocoded_address_params
      params.require(:geocoded_address).permit(:location, :latitude, :longitude, :address_street, :address_city, :address_state, :address_postal, :address_country)
    end
end
