class NewslettersController < ApplicationController
  before_action :set_newsletter, only: [:show, :edit, :update, :destroy]

  # GET /newsletters
  # GET /newsletters.json
  def index
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @newsletters = Newsletter.all
    end
  end

  # GET /newsletters/1
  # GET /newsletters/1.json
  def show
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    end
  end

  # GET /newsletters/new
  def new
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @newsletter = Newsletter.new
    end
  end

  # GET /newsletters/1/edit
  def edit
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    end
  end

  # POST /newsletters
  # POST /newsletters.json
  def create
    @newsletter = Newsletter.new(newsletter_params)

    respond_to do |format|
      if @newsletter.save
        flash[:notice] = 1
        format.html { redirect_to root_url, notice: "You have been successfully registered" }
        format.json { render :show, status: :created, location: @newsletter }
      else
        format.html { redirect_to root_url, notice: ("#{@newsletter.errors.full_messages[0]}" || "") }
        format.json { render json: @newsletter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /newsletters/1
  # PATCH/PUT /newsletters/1.json
  def update
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      respond_to do |format|
        if @newsletter.update(newsletter_params)
          format.html { redirect_to @newsletter, notice: 'Newsletter was successfully updated.' }
          format.json { render :show, status: :ok, location: @newsletter }
        else
          format.html { render :edit }
          format.json { render json: @newsletter.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /newsletters/1
  # DELETE /newsletters/1.json
  def destroy
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @newsletter.destroy
      respond_to do |format|
        format.html { redirect_to newsletters_url, notice: 'Newsletter was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_newsletter
      @newsletter = Newsletter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def newsletter_params
      params.require(:newsletter).permit(:name, :email, :postal)
    end
end
