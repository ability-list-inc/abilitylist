class PasswordsController < Devise::PasswordsController
  protected
  def after_sending_reset_password_instructions_path_for(resource_name)
    #return your path
    #flash[:success] = "Check your inbox for a link to reset your password"
    return "/passwords/reset_sent"
    #return false
  end
end