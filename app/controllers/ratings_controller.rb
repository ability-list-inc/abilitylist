class RatingsController < ApplicationController
  before_action :set_rating, only: [:show, :edit, :update, :destroy]

  # GET /ratings
  # GET /ratings.json
  def index
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @ratings = Rating.all
    end
  end

  # GET /ratings/1
  # GET /ratings/1.json
  def show
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    end
  end

  # GET /ratings/new
  def new
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @rating = Rating.new
    end
  end

  # GET /ratings/1/edit
  def edit
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    end
  end

  # POST /ratings
  # POST /ratings.json
  def create
    @rating = Rating.new(rating_params)

    if current_user.can_favorite_resource(@rating.user_id)
      respond_to do |format|
        if @rating.save
          Resource.find(@rating.resource_id).update_average_rating
          format.html { redirect_to "/resources/" + @rating.resource_id.to_s, notice: 'Rating was successfully created.' }
          format.json { render :show, status: :created, location: @rating }
        else
          format.html { redirect_to "/resources/" + @rating.resource_id.to_s }
          format.json { render json: @rating.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /ratings/1
  # PATCH/PUT /ratings/1.json
  def update
    if current_user.can_favorite_resource(@rating.user_id)
      respond_to do |format|
        if @rating.update(rating_params)
          Resource.find(@rating.resource_id).update_average_rating
          format.html { redirect_to "/resources/" + @rating.resource_id.to_s, notice: 'Rating was successfully updated.' }
          format.json { render :show, status: :ok, location: @rating }
        else
          format.html { redirect_to "/resources/" + @rating.resource_id.to_s }
          format.json { render json: @rating.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /ratings/1
  # DELETE /ratings/1.json
  def destroy
    if current_user.can_favorite_resource(@rating.user_id)
      resource_id = @rating.resource_id
      @rating.destroy
      Resource.find(resource_id).update_average_rating
      respond_to do |format|
        format.html { redirect_to "/resources/" + resource_id.to_s, notice: 'Rating was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rating
      @rating = Rating.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rating_params
      params.require(:rating).permit(:user_id, :resource_id, :score, :comment)
    end
end
