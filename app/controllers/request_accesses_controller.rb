class RequestAccessesController < ApplicationController
  before_action :set_request_access, only: [:show, :edit, :update, :destroy]

  # GET /request_accesses
  # GET /request_accesses.json
  def index
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @request_accesses = RequestAccess.all
    end
  end

  # GET /request_accesses/1
  # GET /request_accesses/1.json
  def show
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    end
  end

  # GET /request_accesses/new
  def new
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @request_access = RequestAccess.new
    end
  end

  # GET /request_accesses/1/edit
  def edit
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    end
  end

  # POST /request_accesses
  # POST /request_accesses.json
  def create
    @request_access = RequestAccess.new(request_access_params)

    if current_user.can_favorite_resource(@request_access.user_id)
      respond_to do |format|
        if @request_access.save
          format.html { redirect_to "/resources/" + @request_access.resource_id.to_s, notice: 'Request access was successfully created.' }
          format.json { render :show, status: :created, location: @request_access }
        else
          format.html { redirect_to "/resources/" + @request_access.resource_id.to_s }
          format.json { render json: @request_access.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /request_accesses/1
  # PATCH/PUT /request_accesses/1.json
  def update
    if current_user.can_favorite_resource(@request_access.user_id)
      respond_to do |format|
        if @request_access.update(request_access_params)
          format.html { redirect_to "/resources/" + @request_access.resource_id.to_s, notice: 'Request access was successfully updated.' }
          format.json { render :show, status: :ok, location: @request_access }
        else
          format.html { redirect_to "/resources/" + @request_access.resource_id.to_s }
          format.json { render json: @request_access.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /request_accesses/1
  # DELETE /request_accesses/1.json
  def destroy
    if current_user.can_favorite_resource(@request_access.user_id)
      resource_id = @request_access.resource_id
      @request_access.destroy
      respond_to do |format|
        format.html { redirect_to "/resources/" + resource_id.to_s, notice: 'Request access was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request_access
      @request_access = RequestAccess.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def request_access_params
      params.require(:request_access).permit(:user_id, :resource_id, :explanation, :bootsy_image_gallery_id)
    end
end
