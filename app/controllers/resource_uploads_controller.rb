class ResourceUploadsController < ApplicationController
  before_action :set_resource_upload, only: [:show, :edit, :update, :destroy, :crop]
  before_action :verify_access, only: [:edit, :update, :destroy, :crop]

  # GET /resource_uploads
  # GET /resource_uploads.json
  def index
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @resource_uploads = ResourceUpload.all
    end
  end

  # GET /resource_uploads/1
  # GET /resource_uploads/1.json
  def show
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    end
  end

  def crop

  end

  # GET /resource_uploads/new
  def new
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @resource_upload = ResourceUpload.new
    end
  end

  # GET /resource_uploads/1/edit
  def edit
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    end
  end

  # POST /resource_uploads
  # POST /resource_uploads.json
  def create
    @resource_upload = ResourceUpload.new(resource_upload_params)

    if current_user.can_edit_resource(Resource.find(@resource_upload.resource_id).user_id)
      respond_to do |format|
        if @resource_upload.save
          format.html { redirect_to "/resources/" + @resource_upload.resource_id.to_s + "/photo", notice: 'Resource upload was successfully created.' }
          format.json { render :show, status: :created, location: @resource_upload }
        else
          format.html { redirect_to "/resources/" + @resource_upload.resource_id.to_s + "/photo" }
          format.json { render json: @resource_upload.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /resource_uploads/1
  # PATCH/PUT /resource_uploads/1.json
  def update
    if current_user.can_edit_resource(Resource.find(@resource_upload.resource_id).user_id)
      respond_to do |format|
        if @resource_upload.update(resource_upload_params)
          format.html { redirect_to "/resources/" + @resource_upload.resource_id.to_s + "/photo", notice: 'Resource upload was successfully updated.' }
          format.json { render :show, status: :ok, location: @resource_upload }
        else
          format.html { redirect_to "/resources/" + @resource_upload.resource_id.to_s + "/photo" }
          format.json { render json: @resource_upload.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /resource_uploads/1
  # DELETE /resource_uploads/1.json
  def destroy
    if current_user.can_edit_resource(Resource.find(@resource_upload.resource_id).user_id)
      resource_id = @resource_upload.resource_id
      @resource_upload.destroy
      respond_to do |format|
        format.html { redirect_to "/resources/" + resource_id.to_s + "/photo", notice: 'Resource upload was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    def verify_access
      unless user_signed_in?
          flash[:notice] = 23
          redirect_to '/users/sign_in'
      else
        unless current_user.can_edit_resource(Resource.find(@resource_upload.resource_id).user_id)

        else
          # insufficient permissions, redirect
        end
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_resource_upload
      @resource_upload = ResourceUpload.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resource_upload_params
      params.require(:resource_upload).permit(:resource_id, :photo, :photo_original_w, :photo_original_h, :photo_box_w, :photo_crop_x, :photo_crop_y, :photo_crop_w, :photo_crop_h, :photo_aspect)
    end
end
