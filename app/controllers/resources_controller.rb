class ResourcesController < ApplicationController
  before_action :set_resource, only: [:show, :edit, :update, :update_weblinks, :destroy, :categories, :weblinks, :photo, :success, :rerouter]
  before_action :verify_access, only: [:edit, :update, :destroy, :update_weblinks, :categories, :weblinks, :photo, :success]
  autocomplete :tag, :name, :class_name => 'ActsAsTaggableOn::Tag', :full => true # <- New
  impressionist

  # GET /resources
  # GET /resources.json
  def index
    unless user_signed_in? and current_user.permissions != 0
      redirect_to "/no_access"
      return
    else
      @resources = Resource.all
      respond_to do |format|
        format.html
        format.json { render json: ResourceDatatable.new(view_context) }
      end
    end
  end

  def rerouter

    redirect_to "/r/#{@resource.id}/#{CGI.escape @resource.name.gsub(/[^a-z0-9\s]/i, '').rstrip }"

  end

  # GET /resources/1
  # GET /resources/1.json
  def show

    # if we haven't already, tweet that this resource was created
    #if Rails.env.production? and ENV['is_staging'] != "true"
    #  if @resource.tweeted == false
    #    begin
    #      $twitter.update("Just uploaded: '#{@resource.name}' https://abilitylist.org/resources/#{@resource.id}")
    #    rescue
    #    end
    #    @resource.tweeted = true
    #    @resource.save!
    #  end
    #end

    # set meta tags for this view
    @meta_title = "#{@resource.name} on AbilityList"
    if @resource.resource_upload
      @meta_img_url = @resource.resource_upload.photo.url(:medium)
    end



    if user_signed_in?
      # retrieve the rating, if it doesn't exist make a new one
      ratings = Rating.where(resource_id: @resource.id, user_id: current_user.id)
      if ratings.length > 0
        @rating = ratings[0]
      else
        @rating = Rating.new
      end

      # retrieve the open request_access, if it doesn't exist make a new one
      access_requests = RequestAccess.where(resource_id: @resource.id, user_id: current_user.id, closed: 0)
      if access_requests.length > 0
        @request_access = access_requests[0]
      else
        @request_access = RequestAccess.new
      end

      # retrieve flag, if it doesn't exist make a new one
      flags = Flag.where(resource_id: @resource.id, user_id: current_user.id)
      if flags.length > 0
        @flag = flags[0]
      else
        @flag = Flag.new
      end
    end

    # format website
    if @resource.website.presence
      if @resource.website.include? "http://" or @resource.website.include? "https://"
        @resource_website = @resource.website
      else
        @resource_website = "http://" + @resource.website
      end
      if @resource_website.length > 26
        @resource_website_short = @resource.website[0..25] + "..."
      else
        @resource_website_short = @resource.website
      end
    end

    # format description
    if @resource.description.length > 400
      i = 390
      while i < @resource.description.length - 1
        if @resource.description[i] == "." or @resource.description[i] == "!" or @resource.description[i] == "?"
          @description_start = @resource.description[0..i]
          @description_end = @resource.description[i+1..-1]
          break
        end
        i = i + 1
      end
      # unlikely, but we may not have found a clean place to break
      @description_start = (@description_start || @resource.description[0..389])
      @description_end = (@description_end || @resource.description[390..-1])
    end

    # format the average rating, remember that resources without ratings are marked with -1.0 db entry
    @average = @resource.current_average_rating
    if @average == -1
      @average = 0
    end

    @related_resources = Resource.where(status: 1).where.not(id: @resource.id).limit(3)
  end

  # GET /resources/new
  def new
    if user_signed_in?
      @resource = Resource.new
    else
      flash[:notice] = 23
      redirect_to '/users/sign_in'
    end
  end

  def clarifyContent
    if user_signed_in?
      @resource = Resource.new
    else
      flash[:notice] = 23
      redirect_to '/users/sign_in'
    end
  end

   # GET /resources/:id/categories
  def categories
    if current_user.can_edit_resource(@resource.user_id)
      @life_situations = Tag.where(life_situation: true)

      @non_profit_services = Tag.where(nonprofit_services: true)
      @one_time = Tag.where(one_time: true)
      @jobs = Tag.where(jobs: true)
      @housing = Tag.where(housing: true)
      @for_profit_services = Tag.where(forprofit_services: true)
      @marketplace = Tag.where(marketplace: true)

      @current_tags = @resource.tags


      if @resource.categories_complete == false
        @resource.update_attributes(:categories_complete => true)
      end
    else
      redirect_to "/no_access"
      return
    end
  end

   # GET /resources/:id/weblinks
  def weblinks
    if current_user.can_edit_resource(@resource.user_id)
      if @resource.weblinks_complete == false
        @resource.update_attributes(:weblinks_complete => true)
      end
    else
      redirect_to "/no_access"
      return
    end
  end

   # GET /resources/:id/photo
  def photo

    if current_user.can_edit_resource(@resource.user_id)
      unless @resource.resource_upload
        @resource_upload = ResourceUpload.new
      else
        @resource_upload = @resource.resource_upload
      end


      if @resource.photo_complete == false
        @resource.update_attributes(:photo_complete => true)
      end
    else
      redirect_to "/no_access"
      return
    end
  end

   # GET /resources/:id/success
  def success
    unless current_user.can_edit_resource(@resource.user_id)
      redirect_to "/no_access"
      return
    end
  end

  # GET /resources/1/edit
  def edit
    unless current_user.can_edit_resource(@resource.user_id)
      redirect_to "/no_access"
      return
    end
  end

  # POST /resources
  # POST /resources.json
  def create
    if current_user.permissions == 0
      @resource = current_user.resources.new(resource_params)
    else
      @resource = Resource.new(resource_params)
    end

    if current_user.can_edit_resource(@resource.user_id)
      respond_to do |format|
        if @resource.save

          update_user_resource_count(@resource.user_id)

          format.html { redirect_to "/resources/" + @resource.id.to_s + "/categories", notice: 'Resource was successfully created.' }
          format.json { render :show, status: :created, location: @resource }
        else
          format.html { render :new }
          format.json { render json: @resource.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /resources/1
  # PATCH/PUT /resources/1.json
  def update

    # we keep a string copy of the tags so that we can search over them easily
    modified_params = resource_params.except(:action, :controller)
    modified_params[:tag_list_string] = modified_params[:tag_list]

    if current_user.can_edit_resource(@resource.user_id)
      respond_to do |format|
        if @resource.update(modified_params)
          if modified_params[:route_to] == "weblinks"
            format.html { redirect_to "/resources/" + @resource.id.to_s + "/weblinks", notice: 'Resource was successfully updated.' }
          else
            format.html { redirect_to "/resources/" + @resource.id.to_s + "/categories", notice: 'Resource was successfully updated.' }
          end
          format.json { render :show, status: :ok, location: @resource }
        else
          format.html { render :edit }
          format.json { render json: @resource.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  # PATCH/PUT /resources/1/weblinks
  # PATCH/PUT /resources/1/weblinks.json
  def update_weblinks
    if current_user.can_edit_resource(@resource.user_id)
      respond_to do |format|
        if @resource.update(resource_params)
          format.html { redirect_to "/resources/" + @resource.id.to_s + "/photo" }
          format.json { render :show, status: :ok, location: @resource }
        else
          format.html { render :edit }
          format.json { render json: @resource.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  # DELETE /resources/1
  # DELETE /resources/1.json
  def destroy
    if current_user.can_edit_resource(@resource.user_id)

      user_id = @resource.user_id

      @resource.destroy
      respond_to do |format|

        update_user_resource_count(user_id)

        format.html { redirect_to "/users/#{current_user.id}", notice: 'Resource was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  private
    def verify_access
      unless user_signed_in?
          flash[:notice] = 23
          redirect_to '/users/sign_in'
      else
        unless current_user.can_edit_resource(@resource.user_id)

        else
          # insufficient permissions, redirect
        end
      end
    end


    def update_user_resource_count(id)

      begin
        count = Resource.where(user_id: id).length
        User.find(id).update_resource_count(count)
      rescue

      end

    end


    # Use callbacks to share common setup or constraints between actions.
    def set_resource
      @resource = Resource.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resource_params
      params.require(:resource).permit(:name, :email, :validated_city, :validated_postal, :validated_country, :user_id, :bootsy_image_gallery_id, :current_average_rating, :address_street, :address_city, :address_state, :address_postal, :latitude, :longitude, :website, :description, :nationwide, :status, :phone, :views, :basics_complete, :categories_complete, :weblinks_complete, :photo_complete, :facebook, :twitter, :linkedin, :yelp, :googleplus, :foursquare, :tweeted, :anonymous, :tag_list, :route_to, :tag_list_string)
    end
end
