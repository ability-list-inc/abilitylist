class ResourcesTagsController < ApplicationController
  before_action :set_resources_tag, only: [:show, :edit, :update, :destroy]

  # GET /resources_tags
  # GET /resources_tags.json
  def index
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @resources_tags = ResourcesTag.all
    end
  end

  # GET /resources_tags/1
  # GET /resources_tags/1.json
  def show
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    end
  end

  # GET /resources_tags/new
  def new
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @resources_tag = ResourcesTag.new
    end
  end

  # GET /resources_tags/1/edit
  def edit
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else

    end
  end

  # POST /resources_tags
  # POST /resources_tags.json
  def create

    @resources_tag = ResourcesTag.new(resources_tag_params)

    if current_user.can_manage_tags_for_resource(Resource.find(@resources_tag.resource_id))
      respond_to do |format|
        if @resources_tag.save
          format.html { redirect_to @resources_tag, notice: 'Resources tag was successfully created.' }
          format.json { render :show, status: :created, location: @resources_tag }
        else
          format.html { render :new }
          format.json { render json: @resources_tag.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def custom_create

    @resources_tag = ResourcesTag.new(resource_id: params[:resource_id].to_i, tag_id: params[:tag_id].to_i)

    if current_user.can_manage_tags_for_resource(Resource.find(@resources_tag.resource_id))
      respond_to do |format|
        if @resources_tag.save
          format.html { redirect_to @resources_tag, notice: 'Resources tag was successfully created.' }
          format.json { render :show, status: :created, location: @resources_tag }
        else
          format.html { render :new }
          format.json { render json: @resources_tag.errors, status: :unprocessable_entity }
        end
      end
    end

  end

  # PATCH/PUT /resources_tags/1
  # PATCH/PUT /resources_tags/1.json
  def update

    if current_user.can_manage_tags_for_resource(Resource.find(@resources_tag.resource_id))
      respond_to do |format|
        if @resources_tag.update(resources_tag_params)
          format.html { redirect_to @resources_tag, notice: 'Resources tag was successfully updated.' }
          format.json { render :show, status: :ok, location: @resources_tag }
        else
          format.html { render :edit }
          format.json { render json: @resources_tag.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /resources_tags/1
  # DELETE /resources_tags/1.json
  def destroy
    if current_user.can_manage_tags_for_resource(Resource.find(@resources_tag.resource_id))
      @resources_tag.destroy
      respond_to do |format|
        format.html { redirect_to resources_tags_url, notice: 'Resources tag was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  # DELETE /resources_tags/1
  # DELETE /resources_tags/1.json
  def custom_destroy

    @resources_tag = ResourcesTag.where(resource_id: params[:resource_id].to_i, tag_id: params[:tag_id].to_i)
    @resources_tag = @resources_tag[0]

    if current_user.can_manage_tags_for_resource(Resource.find(@resources_tag.resource_id))
      @resources_tag.destroy
      respond_to do |format|
        format.html { redirect_to resources_tags_url, notice: 'Resources tag was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_resources_tag
      @resources_tag = ResourcesTag.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resources_tag_params
      params.require(:resources_tag).permit(:resource_id, :tag_id)
    end
end
