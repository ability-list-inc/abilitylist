class SavedsController < ApplicationController
  before_action :set_saved, only: [:show, :edit, :update, :destroy]

  # GET /saveds
  # GET /saveds.json
  def index
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @saveds = Saved.all
    end
  end

  # GET /saveds/1
  # GET /saveds/1.json
  def show
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    end
  end

  # GET /saveds/new
  def new
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @saved = Saved.new
    end 
  end

  # GET /saveds/1/edit
  def edit
    if current_user.can_favorite_resource(@saved.user_id)

    else
      redirect_to "/no_access"
      return
    end
  end

  # POST /saveds
  # POST /saveds.json
  def create
    @saved = Saved.new(saved_params)

    if current_user.can_favorite_resource(@saved.user_id)
      respond_to do |format|
        if @saved.save
          format.html { redirect_to @saved, notice: 'Saved was successfully created.' }
          format.json { render :show, status: :created, location: @saved }
        else
          format.html { render :new }
          format.json { render json: @saved.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  def custom_create

    # check to see if there already exists a saved relation for these values
    unless Saved.where(resource_id: params[:resource_id].to_i, user_id: params[:user_id].to_i).length > 0
      @saved = Saved.new(resource_id: params[:resource_id].to_i, user_id: params[:user_id].to_i)

      # if the provided user id matches the logged in (valid) user, permissions check out
      if current_user.can_favorite_resource(@saved.user_id)
        respond_to do |format|
          if @saved.save
            format.html { redirect_to @saved, notice: 'Saved was successfully created.' }
            format.json { render :show, status: :created, location: @saved }
          else
            format.html { render :new }
            format.json { render json: @saved.errors, status: :unprocessable_entity }
          end
        end
      else
        redirect_to "/no_access"
        return
      end
    end
  end

  # PATCH/PUT /saveds/1
  # PATCH/PUT /saveds/1.json
  def update
    if current_user.can_favorite_resource(@saved.user_id)
      respond_to do |format|
        if @saved.update(saved_params)
          format.html { redirect_to @saved, notice: 'Saved was successfully updated.' }
          format.json { render :show, status: :ok, location: @saved }
        else
          format.html { render :edit }
          format.json { render json: @saved.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  # DELETE /saveds/1
  # DELETE /saveds/1.json
  def destroy
    if current_user.can_favorite_resource(@saved.user_id)
      @saved.destroy
      respond_to do |format|
        format.html { redirect_to saveds_url, notice: 'Saved was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  def custom_destroy

    @saved = Saved.where(resource_id: params[:resource_id].to_i, user_id: params[:user_id].to_i)
    @saved = @saved[0]

    # we may only delete if this saved resource entry belongs to the logged in user
    if current_user.can_favorite_resource(@saved.user_id)
      @saved.destroy
      respond_to do |format|
        format.html { redirect_to saveds_url, notice: 'Resources tag was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to "/no_access"
      return
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_saved
      @saved = Saved.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def saved_params
      params.require(:saved).permit(:user_id, :resource_id)
    end
end
