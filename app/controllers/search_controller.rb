require 'actionpack/action_caching'
require 'digest/sha1'
class SearchController < ApplicationController
  caches_action :index, :layout => false, :if => proc { request.fullpath != "/search" and request.fullpath != "/search?utf8=%E2%9C%93&location=&commit=SEARCH" }, :cache_path => proc {|c|
    timestamp = Resource.order("updated_at").last.updated_at
    #logger.debug("C is : #{request.fullpath}")
    string = timestamp.to_s + (c.params.inspect || "")
    {:tag => Digest::SHA1.hexdigest(string)}
  }
  impressionist



  def new_search

    if params[:location].present?
      @location = params[:location]
    end
    if params[:tag].present?
      @tag = params[:tag]
    end

    @most_used_tags = ActsAsTaggableOn::Tag.most_used()

  end

  def index
  	#logger.debug "LOGGING PARAMS: #{location}"
  	# let's start by assuming there is a value of the location string
  	@search_layout = true

    # the first thing we wanna do is check to see if any tag filters have been included
    # if there are, we will store the ids in this array
    tag_ids = Array.new
    params.each do |key, value|
      # target groups using regular expressions
      if (key.include? "tag_")
        #logger.debug"KEY: #{key}"
        temp = key.dup
        #logger.debug"TEMP: #{temp}"
        tag_ids.push((temp.sub( "tag_", "" )).to_i)
      end
    end


  	# idiomatic nil checks
  	@location = (params[:location] || '')
    @distance_specified = (params[:distance] || "nope")
  	@distance = (params[:distance] || 10)
    @nationwide = (params[:nationwide] || false)
  	@per_page = (params[:per_page] || 15)
    @with_reviews = (params[:with_reviews] || false)
    @with_reviews = @with_reviews.to_bool
    # logger.debug "WITH REVIEWS: #{params[:with_reviews]} and #{@with_reviews}"
    @average_rating = (params[:average_rating] || 0.0) # possible index
    if @average_rating.to_f > 0.0
      @with_reviews = true
    end
    #logger.debug "AVERAGE RATING: #{params[:average_rating]} and #{@average_rating} and WITH REVIEWS #{@with_reviews}"

  	# if it was nil, we need to give it something, go with the user's location if signed in
  	# if not signed in, default to something
  	if @location.length < 1 or @location.blank?
  		if user_signed_in?
        #for some freak accident where a user doesn't have an address
  			@location = (current_user.full_street_address.strip || "10003")
  		else
  			#default location?
  			@location = "10003"
  		end
  	end

    # check if the input location is in an area abilitylist servers (just for message sake)
    c = Checker.create(full_address: @location)
    if c.status == 0
      @out_of_area = false
    else
      @out_of_area = true
    end
    c.destroy

    # if nationwide
    #if @nationwide != false
    #  @results = Resource.where(nationwide: 1).filter_with_reviews(@with_reviews).paginate(page: params[:page], per_page: @per_page)
    #  @slider_value = 10


    # if we got tag filters, pull in the applicable relations models
    if tag_ids.length > 0
      resource_tags = ResourcesTag.where(tag_id: tag_ids)

      # from the relations, create an array of resource_ids
      resource_ids = Array.new
      resource_tags.each do |rt|
        resource_ids.push(rt.resource_id)
      end


      # we want unique ids
      resource_ids = resource_ids.uniq

      #logger.debug "RESOURCE IDS: #{resource_ids.inspect}"


      ########## SEARCHING WITH (AT LEAST ONE) TAG FILTER ################

      #@results = scope.near(@location, @distance).paginate(page: params[:page], per_page: @per_page)
      # if this returns nil, we should increase the diameter
      @results = Resource.in_id_array(resource_ids).filter_by_nationwide(@nationwide).filter_with_reviews(@with_reviews).filter_by_average_rating(@with_reviews, @average_rating).near(@location, @distance).paginate(page: params[:page], per_page: @per_page)

      unless (@distance_specified == "nope" and @results.length == 0) or @distance.to_i > 150
        try_distances = [1, 2, 3, 4, 5, 7, 10, 15, 25, 50, 75, 100, 150]
      else
        try_distances = [25, 50, 100, 300, 500, 1000, 2500, 5000]
      end

      i = 0
      while i < try_distances.length
        if @distance_specified == "nope" and @results.length == 0
          @results = Resource.in_id_array(resource_ids).filter_by_nationwide(@nationwide).filter_with_reviews(@with_reviews).filter_by_average_rating(@with_reviews, @average_rating).near(@location, try_distances[i]).paginate(page: params[:page], per_page: @per_page)
          @distance = try_distances[i]
          i += 1
        else
          break
        end
      end
      hash = Hash[try_distances.map.with_index.to_a]
      @slider_value = (@slider_value || hash[@distance.to_i])

      ########## END ################


    else

      ########## SEARCHING WITH NO (ZERO) TAG FILTERS ################

      #@results = scope.near(@location, @distance).paginate(page: params[:page], per_page: @per_page)
    	# if this returns nil, we should increase the diameter
      @results = Resource.filter_by_nationwide(@nationwide).filter_with_reviews(@with_reviews).filter_by_average_rating(@with_reviews, @average_rating).near(@location, @distance).paginate(page: params[:page], per_page: @per_page)

      unless (@distance_specified == "nope" and @results.length == 0) or @distance.to_i > 150
        try_distances = [1, 2, 3, 4, 5, 7, 10, 15, 25, 50, 75, 100, 150]
      else
        try_distances = [25, 50, 100, 300, 500, 1000, 2500, 5000]
      end

      i = 0
      while i < try_distances.length
        if @distance_specified == "nope" and @results.length == 0
          @results = Resource.filter_by_nationwide(@nationwide).filter_with_reviews(@with_reviews).filter_by_average_rating(@with_reviews, @average_rating).near(@location, try_distances[i]).paginate(page: params[:page], per_page: @per_page)
          @distance = try_distances[i]
          i += 1
        else
          break
        end
      end
      hash = Hash[try_distances.map.with_index.to_a]
      @slider_value = (@slider_value || hash[@distance.to_i])

      ########## END ################
    end


    # pull in all the tags
    @life_situations = Tag.where(life_situation: true)

    @non_profit_services = Tag.where(nonprofit_services: true)
    @one_time = Tag.where(one_time: true)
    @jobs = Tag.where(jobs: true)
    @housing = Tag.where(housing: true)
    @for_profit_services = Tag.where(forprofit_services: true)
    @marketplace = Tag.where(marketplace: true)
  end
  def classic_driver

    respond_to do |format|
      format.html
      format.json { render json: ClassicDatatable.new(view_context, {location: params[:location]}) }
    end

  end
  def people_data

    respond_to do |format|
      format.html
      format.json { render json: PeopleDatatable.new(view_context, {location: params[:location]}) }
    end

  end
  def search

  end
  def classic

  end

end
