class StaticController < ApplicationController
  impressionist

  def landing
    # styling is a tiny bit different using this flag
    @landing = true
  end


  def signupraffle

    # we choose a different title and image that will be used in the facebook sharer
    @meta_title = "Sign up for AbilityList.org and enter to win $25 Amazon Gift Card"
    @meta_img_url = "https://abilitylist.org/img/igg/laptop-transparent.png"

    # count the current total entries
    @entries = User.where(:created_at => DateTime.new(2015,7,22,1,0,0)..DateTime.new(2015,8,1,23,59,0)).count

  end

  def submit_resource

    begin
      logger.debug "#{params.inspect}"
      SuggestedResource.send_to_admin(params[:title], params[:url], params[:name], params[:email]).deliver
      redirect_to "/", notice: "Thank you for your submission #{params[:name]}."
    rescue

      redirect_to "/", :flash => { :warning => 'We were unable to process this submission. Please try again later.' }
    end

  end

  def about
    # styling is a tiny bit different using this flag
    @about = true
  end

  def api
    # styling is a tiny bit different using this flag
    @about = true
  end

  def thankyou
    # home to IGG thank you page found in /views/static/thankyou
  end

  def no_access
    # some 'verify_user' methods point to this page when a user shouldn't be accessing them
  end

  def reset_sent
    # this page is hit after a user sends a password reset request
  end

  def accessibility
    # this page explains our approach to web accessibility
  end

  def donate
    # reroutes url requests to the hashtag link dontation widget from Rainmaker
    redirect_to "/#/my/donate"
  end
end
