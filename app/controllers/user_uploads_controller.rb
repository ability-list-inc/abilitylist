class UserUploadsController < ApplicationController
  before_action :set_user_upload, only: [:show, :edit, :update, :destroy, :crop]
  before_action :verify_access, only: [:edit, :update, :destroy, :crop]

  # GET /user_uploads
  # GET /user_uploads.json
  def index
    unless user_signed_in? and current_user.permissions != 0 
      redirect_to "/no_access"
      return
    else
      @user_uploads = UserUpload.all
    end
  end

  # GET /user_uploads/1
  # GET /user_uploads/1.json
  def show
    unless current_user.can_edit_user_with_id(@user_upload.user_id)
      redirect_to "/no_access"
      return
    end
  end

  def crop

  end

  # GET /user_uploads/new
  def new
    if user_signed_in?
      @user_upload = UserUpload.new
    else
      flash[:notice] = 23
      redirect_to '/users/sign_in'
    end
  end

  # GET /user_uploads/1/edit
  def edit
    unless current_user.can_edit_user_with_id(@user_upload.user_id)
      redirect_to "/no_access"
      return
    end
  end

  # POST /user_uploads
  # POST /user_uploads.json
  def create
    @user_upload = UserUpload.new(user_upload_params)

    # check to see that this user has the correct access privileges 
    if current_user.can_edit_user_with_id(@user_upload.user_id)
      respond_to do |format|
        if @user_upload.save
          format.html { redirect_to edit_user_upload_path(@user_upload), notice: 'User upload was successfully created.' }
          format.json { render :show, status: :created, location: @user_upload }
        else
          format.html { render :new }
          format.json { render json: @user_upload.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  # PATCH/PUT /user_uploads/1
  # PATCH/PUT /user_uploads/1.json
  def update
    if current_user.can_edit_user_with_id(@user_upload.user_id)
      respond_to do |format|
        if @user_upload.update(user_upload_params)
          format.html { redirect_to edit_user_upload_path(@user_upload), notice: 'User upload was successfully updated.' }
          format.json { render :show, status: :ok, location: @user_upload }
        else
          format.html { redirect_to edit_user_upload_path(@user_upload) }
          format.json { render json: @user_upload.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  # DELETE /user_uploads/1
  # DELETE /user_uploads/1.json
  def destroy
    if current_user.can_edit_user_with_id(@user_upload.user_id)
      user_id = @user_upload.user_id.to_s
      @user_upload.destroy
      respond_to do |format|
        format.html { redirect_to "/user_uploads/new", notice: 'User upload was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  private

    def verify_access
      unless user_signed_in?
          flash[:notice] = 23
          redirect_to '/users/sign_in'
      else
        unless current_user.can_edit_user_with_id(@user_upload.user_id)
          redirect_to "/no_access"
          return
        end
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user_upload
      @user_upload = UserUpload.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_upload_params
      params.require(:user_upload).permit(:user_id, :photo, :photo_original_w, :photo_original_h, :photo_box_w, :photo_crop_x, :photo_crop_y, :photo_crop_w, :photo_crop_h, :photo_aspect)
    end
end
