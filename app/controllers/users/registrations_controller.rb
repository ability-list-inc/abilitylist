class Users::RegistrationsController < Devise::RegistrationsController
  def new
    @signup = true
    super
  end
  def create
    # Recaptcha is disabled in test and development environments by default but
    # we must also disable it on our staging server using an env variable
    if verify_recaptcha or ENV['is_staging'] == "true"
      super
    else
      build_resource(sign_up_params)
      clean_up_passwords(resource)
      flash.now[:alert] = "There was an error with the recaptcha code below. Please re-enter the code."
      flash.delete :recaptcha_error
      @signup = true
      render :new
    end
  end
end
