require 'jwt'

class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :conversations]
  autocomplete :epithet, :name, :class_name => 'ActsAsTaggableOn::Tag', :full => true # <- New
  impressionist

  # GET /users
  # GET /users.json
  def index
    unless user_signed_in? and current_user.permissions != 0
      redirect_to "/no_access"
      return
    else
      respond_to do |format|
        format.html
        format.json { render json: UserDatatable.new(view_context) }
      end
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show

  end

  # GET /users/1
  # GET /users/1.json
  def conversations
    @conversations = current_user.conversations
  end

  # GET /users/1
  # GET /users/1.json
  def conversation
    authorized = false # default
    if user_signed_in?
      authorized = ConversationsUser.where([
        "conversation_id = ? and user_id = ?",
        params[:conversation_id], current_user.id
      ]).length === 1 # sets to true if user part of conversation
    end
    if authorized

      # must set this for firefox
      response.headers["Access-Control-Allow-Origin"] = "https://www.googleapis.com"

      # show this user the conversation
      user_jwt = create_custom_token(current_user.id, false)
      conversation = Conversation.find(params[:conversation_id])
      @other_participant = (conversation.users - [current_user]).shift

      chat_cookie_hash = {
        :user_jwt => create_custom_token(current_user.id, false),
        :user_id => current_user.id,
        :chat_room_id => conversation.id,
        :chat_room_token => conversation.access_token,
        :chat_display_name => current_user.full_name,
        :chat_photo_url => current_user.fetch_photo(:thumb),
        :chat_room_environment => Rails.env
      }
      if Rails.env.production?
        cookies[:abilitylistchat] = {
          value: JSON.generate(chat_cookie_hash),
          expires: 1.minute.from_now,
          domain: "abilitylist.org"
        }
      else
        cookies[:abilitylistchat] = {
          value: JSON.generate(chat_cookie_hash),
          expires: 1.minute.from_now
        }
      end
    else
      # this is verboten
      redirect_to "/no_access"
      return
    end
  end

  # GET /users/new
  def new
      @user = User.new
  end

  # GET /users/1/edit
  def edit
    unless current_user.can_edit_user_with_id(params[:id].to_i)
      redirect_to "/no_access"
      return
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if current_user.can_edit_user_with_id(params[:id].to_i)
      respond_to do |format|
        if @user.update(user_params)
          sign_in(@user == current_user ? @user : current_user, :bypass => true)
          format.html { redirect_to @user, notice: 'User was successfully updated.' }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  # GET/PATCH /users/:id/finish_signup
  def finish_signup
    # authorize! :update, @user
    if request.patch? && params[:user] #&& params[:user][:email]
      if @user.update(user_params)
        @user.skip_reconfirmation!
        sign_in(@user, :bypass => true)
        redirect_to @user, notice: 'Your profile was successfully updated.'
      else
        @show_errors = true
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if current_user.can_edit_user_with_id(@user.id)
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to "/no_access"
      return
    end
  end

  private

    # Get your service account's email address and private key from the
    # JSON key file
    $service_account_email = ENV["chat_auth_service_email"]
    $private_key = File.read '/abilitylistchat.pem'
    $private_key = OpenSSL::PKey::RSA.new($private_key)

    def create_custom_token(uid, is_premium_account)
      now_seconds = Time.now.to_i
      payload = {
        :iss => $service_account_email,
        :sub => $service_account_email,
        :aud => 'https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit',
        :iat => now_seconds,
        :exp => now_seconds+(60*60), # Maximum expiration time is one hour
        :uid => uid,
        :claims => {:premium_account => is_premium_account}
      }
      JWT.encode payload, $private_key, 'RS256'
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      accessible = [ :name, :email, :bio, :encrypted_password, :first_name, :last_name, :dob, :permissions, :address_street, :address_city, :address_state, :address_postal, :phone, :reset_password_token, :reset_password_sent_at, :resource_count, :epithet_list, :interested_in, :can_share, :conversation_id ] # extend with your own params
      accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
      params.require(:user).permit(accessible)
    end
end
