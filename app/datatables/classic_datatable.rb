class ClassicDatatable < AjaxDatatablesRails::Base

  def sortable_columns
    @sortable_columns ||= %w(Resource.id Resource.name Distance.distance)
  end

  def searchable_columns
    @searchable_columns ||= %w(Resource.name Resource.tag_list_string)
  end

  private

  # location is passed as an arguement in some searches
  def location
    if options[:location].present?
      options[:location]
    else
      # provide default location
      "10003"
    end
  end

  def haversine_distance_from(lat1, lng1, lat2, lng2)
    # Rather than using a geocoding api with external requests,
    # it is much faster to calculate an estimated distance using
    # the haversine method (length of arc with average curvature of the earth).
    distance = Haversine.distance(lat1, lng1, lat2, lng2)
    return distance.to_miles
  end

  def distance_record_driver(location)
    # check for this location search in the cache, if it's not there geocode it (for lat/lng)
    geocoded_address = Rails.cache.fetch("ga-#{location}") do
      GeocodedAddress.where(location: location).first_or_create
    end

    # Then, so that users may sort by distance, we need to calculate the distance from this location
    # to the location of every resource in the database.
    # find_each: instead of loading every resource into memory this loop loads batches of (default) 1000
    Resource.find_each do |resource|
      # we also cache these distance calculations to spare db hits
      Rails.cache.fetch("#{resource.id}-#{location}") do
        create_distance_record(resource, resource.nationwide.to_bool, geocoded_address)
      end
    end
  end

  def create_distance_record(resource, nationwide, geocoded_address)
    if nationwide == true
      distance = 9999999999
    else
      distance = haversine_distance_from(resource.latitude, resource.longitude, geocoded_address.latitude, geocoded_address.longitude)
    end
    # When a distance record gets moved out of the cache, creating a Distance record that
    # has the same values of an existing Distance record will potentially throw an error
    # because the cache_string column must be unique.
    # Find_or_create_by will only create the record if it does not already exist with supplied args.
    Distance.where(location: location, resource_id: resource.id, distance: distance, cache_string: "#{resource.id}-#{location}-#{distance.round}" ).first_or_create
  end

  def data
    records.map do |record|
      [
        "#{ApplicationController.helpers.format_resource_image(record)}",
        "<a href='/resources/#{record.id}' class='resource-search-title' >#{record.name}</a>",
        "<a href='/resources/#{record.id}' >#{ApplicationController.helpers.make_labels(record.tag_list_string)}</a>",
        "#{ApplicationController.helpers.format_user_avatar(record.user)}",
        "#{ApplicationController.helpers.print_map(record, location)}"
      ]
    end
  end

  def get_raw_records
    # this LEFT OUTER JOIN query allows users to sort by distance quickly and efficiently
    query = "LEFT OUTER JOIN distances ON ( distances.resource_id = resources.id ) AND ( distances.location = '#{location}' )"
    results = Resource.where(status: 1).includes(:user).joins(query)

    # this creates distance records from the given location
    distance_record_driver(location)

    return results
  end
end
