class PeopleDatatable < AjaxDatatablesRails::Base

  def sortable_columns
    @sortable_columns ||= %w(User.id)
    # this is equal to:
    # @sortable_columns ||= ['User.first_name', 'User.last_name', 'User.bio']
  end

  def searchable_columns
    @searchable_columns ||= %w(User.first_name User.last_name User.interested_in User.can_share)
    # this is equal to:
    # @searchable_columns ||= ['User.first_name', 'User.last_name', 'User.bio']
  end

  private

  def data
    records.map do |record|
      [
        "#{ApplicationController.helpers.format_user_avatar(record)}",
        "<a href='/newConversation/#{record.id}' class='blue-link'><i class='fa fa-comments' aria-hidden='true'></i> Chat</a>",
        "<a href='/resources/#{record.id}' >#{ApplicationController.helpers.make_labels(record.epithet_list)}</a>",
        "#{record.interested_in}",
        "#{record.can_share}",
        "#{record.try(:address_state)}"
        # comma separated list of the values for each cell of a table row
        # example: record.attribute,
      ]
    end
  end

  def get_raw_records
    # insert query here
    User.all
  end

  # ==== Insert 'presenter'-like methods below if necessary
end
