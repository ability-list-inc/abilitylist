class ResourceDatatable < AjaxDatatablesRails::Base

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(Resource.id Resource.id Resource.name User.last_name Resource.nationwide Resource.views Resource.address_state Resource.address_postal)
    #@sortable_columns ||= []
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(Resource.name User.last_name User.first_name Resource.nationwide Resource.views Resource.address_state Resource.address_postal)
    #@searchable_columns ||= []
  end

  private
  def humanize_bool(b)
    if b == 1
      return "Yes"
    else
      return "No"
    end
  end
  def data
    records.map do |record|

      if record.resource_upload
      [

        "<img src='#{record.resource_upload.photo.url(:thumb)}'>",
        "<a href='/resources/#{record.id}' >#{record.id}</a>",
        "<a href='/resources/#{record.id}' >#{record.name}</a>",
        "<a href='/users/#{record.user.id}' >#{record.user.last_name + ",<br>" + record.user.first_name}</a>",
        "#{humanize_bool(record.nationwide)}",
        record.views,
        record.address_state,
        record.address_postal,
        '<a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="/resources/#{record.id}">Destroy</a>'


        # comma separated list of the values for each cell of a table row
        # example: record.attribute,
      ]
      else
      [

        "<div style='width: 60px; height: 60px; background: #fff;'></div>",
        "<a href='/resources/#{record.id}' >#{record.id}</a>",
        "<a href='/resources/#{record.id}' >#{record.name}</a>",
        "<a href='/users/#{record.user.id}' >#{record.user.last_name + ",<br>" + record.user.first_name}</a>",
        "#{humanize_bool(record.nationwide)}",
        record.views,
        record.address_state,
        record.address_postal,
        '<a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="/resources/#{record.id}">Destroy</a>'
      ]
      end
    end
  end

  def get_raw_records
    # insert query here
    #Resource.all
    Resource.joins(:user).joins('LEFT OUTER JOIN resource_uploads ON resource_uploads.resource_id = resources.id')

  end

  # ==== Insert 'presenter'-like methods below if necessary
end
