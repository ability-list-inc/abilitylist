class UserDatatable < AjaxDatatablesRails::Base
  include ActionView::Helpers::DateHelper
  #include AjaxDatatablesRails::Extensions::Kaminari

  def sortable_columns
    @sortable_columns ||= %w(User.last_name User.created_at User.email User.resource_count User.sign_in_count User.last_sign_in_at User.address_state User.address_postal)
    # this is equal to:
    # @sortable_columns ||= ['User.first_name', 'User.last_name', 'User.bio']
  end

  def searchable_columns
    @searchable_columns ||= %w(User.first_name User.last_name User.email User.address_postal)
    # this is equal to:
    # @searchable_columns ||= ['User.first_name', 'User.last_name', 'User.bio']
  end

  def format_user_avatar(user)

    result = ""
    if user and user.user_upload
      result += "<div class='nav-profile-photo-wrapper' aria-hidden='true'>"
      result += "<img src='#{user.user_upload.photo.url(:thumb)}' class='resource-search-user-avatar'"
      result += "</div>"
      result += "<p class='resource-search-user-name'>#{user.try(:first_name)}<br> #{user.try(:last_name)}</p>"
    else 
      result += "<div class='nav-profile-photo-wrapper' aria-hidden='true'>"
      result += "<img src='/img/users/avatar.jpg' class='resource-search-user-avatar'>"
      result += "</div>"
      result += "<p class='resource-search-user-name'>#{user.try(:first_name)}<br> #{user.try(:last_name)}</p>"
    end

    return result
  end  

  def format_registered_at_info(user)
    "<a href='/users/#{user.id}' >#{user.created_at.try(:strftime, "%b %d %Y @ %I:%M %p")}<br>(about #{time_ago_in_words(user.created_at)} ago)</a>"
  end

  def format_sign_in_info(user)
    if user.last_sign_in_at
      "#{user.last_sign_in_at.try(:strftime, "%b %d %Y @ %I:%M %p")} <br> (about #{time_ago_in_words(user.last_sign_in_at)} ago)"
    else
      "Never"
    end
  end

  private

  def data
    records.map do |record|
      [
        "#{format_user_avatar(record)}", 
        "#{format_registered_at_info(record)}", 
        "<a href='mailto:#{record.email}' >#{record.email}</a>",
        record.resource_count,
        record.sign_in_count,
        "#{format_sign_in_info(record)}",
        record.address_state,
        record.address_postal


      ]
    end
  end

  def get_raw_records
    # suppose we need all User records
    # Rails 4+
    User.all
    # Rails 3.x
    # User.scoped
  end

  # ==== Insert 'presenter'-like methods below if necessary
end
