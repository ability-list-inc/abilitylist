module ApplicationHelper
  require 'openssl'

    # for flash messages
    def bootstrap_class_for flash_type

      case flash_type
        when "notice"
          "alert alert-success"
        when "success"
          "alert alert-success"
        when "error"
          "alert alert-error"
        when "alert"
          "alert alert-block"
        when "notice"
          "alert alert-info"
        when "warning"
          "alert alert-warning"
        else
          flash_type.to_s
      end
    end

    # captcha requires encription accomplished using openssl
    def encrypt(string)
      padded = pad(string)
      iv="\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00"
      cipher=OpenSSL::Cipher::Cipher.new("AES-128-CBC")
      binkey = '4adcc5c77e92ecf41618b90e766fa9a1'.unpack('a2'*16).map{|x| x.hex}.pack('c'*16)
      cipher.encrypt
      cipher.key=binkey
      cipher.iv=iv
      ciphertext = []
      cipher.padding=0
      ciphertext = cipher.update(padded)
      ciphertext << cipher.final() rescue nil
      Base64.encode64(ciphertext).strip.gsub(/\+/, '-').gsub(/\//, '_').gsub(/\n/,'')
    end

    # pads a string to 16 places
    def pad(str)
      l= 16-(str.length%16)
      l.times do
        str<< l
      end
      str
    end

    # creates a captcha link using an encrypted email
    def mail_hide(email)
      begin
        public_key = '01Ppp0bBDDgcw3aD4NDxbZSQ=='
        encrypted_email = encrypt(email)
        email_snippet = email[0] + "..." + email[email.index('@')..-1]
        return "<a target='_blank' href='JavaScript:newPopup(&#39;http://www.google.com/recaptcha/mailhide/d?k=#{public_key}&c=#{encrypted_email}&#39;);'>#{email_snippet}</a>"
      rescue
        return "<br>"
      end
    end

    # Devise helper method to identify resource as 'User'
    def resource_name
      :user
    end

    # Devise helper method idiomatically check for nil
    def resource
      @resource ||= User.new
    end

    # Devise helper method to attempt to find the mapped route for devise based on request path
    def devise_mapping
      @devise_mapping ||= Devise.mappings[:user]
    end

    def small_map_url(lat, lng, x = 150, y = 100, zoom = 1, scale = 2, marker = false)
      url = "https://maps.googleapis.com/maps/api/staticmap?center=#{lat},#{lng}&zoom=#{zoom}&scale=#{scale}&size=#{x}x#{y}&maptype=roadmap&key=#{ENV['google_maps_api_key']}"
      if marker == true
        url += "&markers=color:green%7Clabel:%7C#{lat},#{lng}"
      end
      return url
    end

    def format_resource_image(record)
      if record.resource_upload
        return "<a href='/resources/#{record.id}'><img class='resource-partial-fitted-image' src='#{record.resource_upload.photo.url(:medium)}'></a>"
      else
        return "<a href='/resources/#{record.id}'><div class='default-resource-search-image'></div></a>"
      end
    end

    def format_user_avatar(user)

      result = "<div class='nav-profile-photo-wrapper' aria-hidden='true'>"
      result += "<a href='/users/#{user.id}'>"

      if user and user.user_upload
        result += "<img src='#{user.user_upload.photo.url(:thumb)}' class='resource-search-user-avatar'"
      else
        result += "<img src='/img/users/avatar.jpg' class='resource-search-user-avatar'>"
      end

      result +="</a>"
      result += "<br><a href='/users/#{user.id}'>#{user.try(:first_name)}<br> #{user.try(:last_name)}</a>"
      result += "</div>"

      return result
    end

    def make_labels(tags_string)

      if tags_string.blank? # nil check
        return ""
      end

      tags = tags_string.split(", ")
      result = ""

      tags.each do |t|
        result += "<button type='button' class='btn btn-sm btn-default btn-margin'>#{t}</button>"
      end

      return result
    end

    def print_map(resource, location)

      # if this location has not been cached we need to geocode for its lat/ lng
      geocoded_address = Rails.cache.fetch("ga-#{location}") do
        GeocodedAddress.create(location: location)
      end

      calculated_distance = haversine_distance_from(resource.latitude, resource.longitude, geocoded_address.latitude, geocoded_address.longitude)

      result = ""
      if resource.nationwide == 1
        result += "<img class='resource-partial-map' src='https://maps.googleapis.com/maps/api/staticmap?center=40.198721,-97.247031&zoom=2&scale=2&size=150x100&maptype=roadmap&key=AIzaSyB7q747xCH7MUXcaRbUUHlViVQXUvRZP34'>"
        result += "<div class='resource-partial-map-caption'>Nationwide</div>"
      else
        result += "<img class='resource-partial-map' src='https://maps.googleapis.com/maps/api/staticmap?center=#{resource.latitude},#{resource.longitude}&zoom=12&scale=2&size=150x100&maptype=roadmap&markers=color:green%7Clabel:%7C#{resource.latitude},#{resource.longitude}&key=AIzaSyB7q747xCH7MUXcaRbUUHlViVQXUvRZP34'>"
        result += "<div class='resource-partial-map-caption'>about #{calculated_distance.round(1)} miles</div>"
      end

      return result

    end

    def haversine_distance_from(lat1, lng1, lat2, lng2)
      # Rather than using a geocoding api with external requests,
      # it is much faster to calculate an estimated distance using
      # the haversine method (length of arc with average curvature of the earth).
      distance = Haversine.distance(lat1, lng1, lat2, lng2)
      return distance.to_miles
    end

    # used for form 'postal_state' select fields throughout the site
    def us_states
      [
        ['State', ''],
        ['Alabama', 'AL'],
        ['Alaska', 'AK'],
        ['Arizona', 'AZ'],
        ['Arkansas', 'AR'],
        ['California', 'CA'],
        ['Colorado', 'CO'],
        ['Connecticut', 'CT'],
        ['Delaware', 'DE'],
        ['District of Columbia', 'DC'],
        ['Florida', 'FL'],
        ['Georgia', 'GA'],
        ['Hawaii', 'HI'],
        ['Idaho', 'ID'],
        ['Illinois', 'IL'],
        ['Indiana', 'IN'],
        ['Iowa', 'IA'],
        ['Kansas', 'KS'],
        ['Kentucky', 'KY'],
        ['Louisiana', 'LA'],
        ['Maine', 'ME'],
        ['Maryland', 'MD'],
        ['Massachusetts', 'MA'],
        ['Michigan', 'MI'],
        ['Minnesota', 'MN'],
        ['Mississippi', 'MS'],
        ['Missouri', 'MO'],
        ['Montana', 'MT'],
        ['Nebraska', 'NE'],
        ['Nevada', 'NV'],
        ['New Hampshire', 'NH'],
        ['New Jersey', 'NJ'],
        ['New Mexico', 'NM'],
        ['New York', 'NY'],
        ['North Carolina', 'NC'],
        ['North Dakota', 'ND'],
        ['Ohio', 'OH'],
        ['Oklahoma', 'OK'],
        ['Oregon', 'OR'],
        ['Pennsylvania', 'PA'],
        ['Puerto Rico', 'PR'],
        ['Rhode Island', 'RI'],
        ['South Carolina', 'SC'],
        ['South Dakota', 'SD'],
        ['Tennessee', 'TN'],
        ['Texas', 'TX'],
        ['Utah', 'UT'],
        ['Vermont', 'VT'],
        ['Virginia', 'VA'],
        ['Washington', 'WA'],
        ['West Virginia', 'WV'],
        ['Wisconsin', 'WI'],
        ['Wyoming', 'WY']
      ]
    end #us_states
end #ApplicationHelper
