class SesMailer < ActionMailer::Base
  default from: 'admin@abilitylist.org'
  def hello_world
    mail(to: 'alz236@nyu.edu', subject: 'I emailed from AmazonSES!')
  end
end