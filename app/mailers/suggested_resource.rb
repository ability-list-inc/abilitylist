class SuggestedResource < ApplicationMailer
  default from: 'admin@abilitylist.org'
  def send_to_admin(title, url, name, email)
    mail(to: 'admin@abilitylist.org', subject: "Suggested resource from #{name} <#{email}>") do |format|
		  format.html {
		    render locals: { title: title, url: url, name: name, email: email }
		  }
		end
  end

end


