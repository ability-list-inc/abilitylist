# == Schema Information
#
# Table name: conversations
#
#  id                   :integer          not null, primary key
#  last_message_sent_at :datetime
#  access_token         :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Conversation < ActiveRecord::Base
  has_many :users, through: :conversations_users
	has_many :conversations_users
  before_create :set_random_access_token

  def set_random_access_token
    o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
    string = (0...50).map { o[rand(o.length)] }.join
    self.access_token = string
  end
end
