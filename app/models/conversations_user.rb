# == Schema Information
#
# Table name: conversations_users
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  conversation_id :integer
#  joined_at       :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class ConversationsUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :conversation
end
