# == Schema Information
#
# Table name: distances
#
#  id           :integer          not null, primary key
#  location     :string
#  resource_id  :integer
#  distance     :float
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  cache_string :string
#

class Distance < ActiveRecord::Base
end
