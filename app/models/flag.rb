# == Schema Information
#
# Table name: flags
#
#  id           :integer          not null, primary key
#  resource_id  :integer
#  user_id      :integer
#  duplicate_id :integer
#  details      :text
#  created_at   :datetime
#  updated_at   :datetime
#  category     :string
#

class Flag < ActiveRecord::Base
	include Bootsy::Container
	belongs_to :user
	belongs_to :resource
end
