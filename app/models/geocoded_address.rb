# == Schema Information
#
# Table name: geocoded_addresses
#
#  id              :integer          not null, primary key
#  location        :string
#  latitude        :float
#  longitude       :float
#  address_street  :string
#  address_city    :string
#  address_state   :string
#  address_postal  :string
#  address_country :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class GeocodedAddress < ActiveRecord::Base
	after_validation :geocode 
	after_validation :reverse_geocode

	geocoded_by :location

	reverse_geocoded_by :latitude, :longitude do |obj, results|
		if geo = results.first
		  # populate your model
		  obj.address_street = geo.address
		  obj.address_city = geo.city
		  obj.address_state = geo.state_code
		  obj.address_postal = geo.postal_code
		  obj.address_country = geo.country_code
		end
	end
end
