# == Schema Information
#
# Table name: newsletters
#
#  id         :integer          not null, primary key
#  name       :string
#  email      :string
#  postal     :string
#  created_at :datetime
#  updated_at :datetime
#

class Newsletter < ActiveRecord::Base

	validates :email, uniqueness: true, :presence => true
	validates :postal, :presence => true


end
