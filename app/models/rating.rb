# == Schema Information
#
# Table name: ratings
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  resource_id :integer
#  comment     :text
#  created_at  :datetime
#  updated_at  :datetime
#  score       :float
#

class Rating < ActiveRecord::Base
	belongs_to :user
	belongs_to :resource
end
