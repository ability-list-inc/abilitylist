# == Schema Information
#
# Table name: request_accesses
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  resource_id :integer
#  explanation :text
#  created_at  :datetime
#  updated_at  :datetime
#  closed      :integer          default(0), not null
#

class RequestAccess < ActiveRecord::Base
	include Bootsy::Container
	belongs_to :user
	belongs_to :resource
end
