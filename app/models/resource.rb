# == Schema Information
#
# Table name: resources
#
#  id                     :integer          not null, primary key
#  name                   :string
#  user_id                :integer
#  address_street         :string
#  address_city           :string
#  address_state          :string
#  address_postal         :string
#  website                :string
#  description            :text
#  nationwide             :integer          default(0)
#  status                 :integer          default(0)
#  phone                  :string
#  created_at             :datetime
#  updated_at             :datetime
#  basics_complete        :boolean          default(FALSE)
#  categories_complete    :boolean          default(FALSE)
#  weblinks_complete      :boolean          default(FALSE)
#  photo_complete         :boolean          default(FALSE)
#  facebook               :string
#  twitter                :string
#  linkedin               :string
#  yelp                   :string
#  googleplus             :string
#  foursquare             :string
#  latitude               :float
#  longitude              :float
#  views                  :integer          default(0), not null
#  current_average_rating :float            default(-1.0), not null
#  email                  :string
#  validated_city         :string
#  validated_postal       :string
#  validated_country      :string
#  tweeted                :boolean          default(FALSE)
#  anonymous              :boolean          default(FALSE)
#  route_to               :string
#  tag_list_string        :text
#

class Resource < ActiveRecord::Base

	#default_scope { where(created_at: 'DESC') }
	#default_scope  { order(:created_at => :desc) }

	belongs_to :user
	has_one :resource_upload, :dependent => :destroy
	has_many :resources_tags, :dependent => :destroy
	has_many :saveds, :dependent => :destroy
	has_many :ratings, :dependent => :destroy
	has_many :request_access, :dependent => :destroy
	has_many :flags, :dependent => :destroy
  has_many :tags, through: :resources_tags
	acts_as_taggable_on :tags

	validates :name, :presence => true
	validates :address_postal, :presence => true, :unless => Proc.new { |r| r.nationwide == 1 }

	geocoded_by :full_street_address   # can also be an IP address
	after_validation :default_postal
	after_validation :geocode          # auto-fetch coordinates
	after_save :clean_tags_string

	reverse_geocoded_by :latitude, :longitude do |obj, results|
		if geo = results.first
		  # populate your model
		  obj.validated_city    = geo.city
		  obj.validated_postal = geo.postal_code
		  obj.validated_country = geo.country_code
		end
	end
	after_validation :reverse_geocode
	after_validation :check_status

	is_impressionable :counter_cache => true, :column_name => :views

	include Bootsy::Container
	
	def clean_tags_string 
		self.tag_list_string = self.tags.join(", ")
	end
	def default_postal
		if self.id != nil and (self.address_postal == nil or self.address_postal == "")
			self.address_postal = "20001"
		end
	end
	def self.in_id_array(array)
		where(id: array)
	end
	def self.all_published
		where(photo_complete: true)
	end
	def self.filter_with_reviews(switch)
		if switch == true
			where.not(current_average_rating: -1.0)
		else
			where(photo_complete: true, status: 0)
		end
	end
	def self.filter_by_nationwide(switch)
		unless switch == false
			where(nationwide: [0,1])
		else
			where(nationwide: 0)
		end
	end
	def self.filter_by_average_rating(with_reviews, score)
		if with_reviews == true
			where("current_average_rating >= ?", score.to_f )
		else
			where("current_average_rating >= ?", -2 )
		end
	end
	def update_average_rating
    # compute average rating
    average = self.ratings.average(:score)
    if average.blank?
      self.current_average_rating = -1
    else
    	self.current_average_rating = average
    end
    self.save
	end

	def check_status
		if self.basics_complete == true and self.categories_complete == true and self.weblinks_complete == true and self.photo_complete == true
			self.status = 1
		else 
			self.status = 0
		end
	end

	def current_user_saved(signed_in, user)
		# that is, has the current user has already favorited this resource?
		if signed_in
			if Saved.where(resource_id: self.id, user_id: user.id).length > 0
				return true
			else
				return false
			end
		else
			return false
		end
	end
	def full_street_address 
		self.address_street + " " + self.address_city + " " + self.address_state + " " + self.address_postal
	end 
	def formatted_distance_from_location(loc)
		figure = self.distance_from(loc).round(1).to_s
		if self.nationwide == 1
			"Nationwide"
		elsif figure != "NaN"
			figure + " miles from you"
		else
			"-"
		end
	end

  def print_map(calculated_distance)

  	resource = self
    result = ""
    if calculated_distance == "Nationwide"
    	result += "<img class='resource-profile-map' src='https://maps.googleapis.com/maps/api/staticmap?center=38.53,-96.9&zoom=3&scale=2&size=285x180&maptype=roadmap&markers=color:green%7Clabel:%7C#{resource.latitude},#{resource.longitude}&key=AIzaSyB7q747xCH7MUXcaRbUUHlViVQXUvRZP34'>"
    else 
    	result += "<img class='resource-profile-map' src='https://maps.googleapis.com/maps/api/staticmap?center=#{resource.latitude},#{resource.longitude}&zoom=12&scale=2&size=285x180&maptype=roadmap&markers=color:green%7Clabel:%7C#{resource.latitude},#{resource.longitude}&key=AIzaSyB7q747xCH7MUXcaRbUUHlViVQXUvRZP34'>"
    end
    result += "<div class='resource-profile-map-caption'>#{calculated_distance}</div>"


    return result

  end

	def formatted_distance_from_user(signed_in, user) 
		if self.nationwide == 1
			"Nationwide"
		elsif signed_in == false
			"-"
		else

			figure = self.distance_from([user.latitude,user.longitude]).round(1).to_s

			if figure != "NaN"
				figure + " miles from you"
			else
				"-"
			end

		end
	end

	private 
end
