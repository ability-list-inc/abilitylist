# == Schema Information
#
# Table name: resource_uploads
#
#  id                 :integer          not null, primary key
#  resource_id        :integer
#  created_at         :datetime
#  updated_at         :datetime
#  photo_file_name    :string
#  photo_content_type :string
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#

class ResourceUpload < ActiveRecord::Base

	has_attached_file :photo, {:styles => { :medium => "200x200#", :thumb => "60x60#", :tiny => "40x40#" }}.merge(Abilitylist::Application.config.paperclip_defaults)

	crop_attached_file :photo

	validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/
	validates_attachment_presence :photo

	belongs_to :resource

end
