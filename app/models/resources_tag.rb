# == Schema Information
#
# Table name: resources_tags
#
#  id          :integer          not null, primary key
#  resource_id :integer
#  tag_id      :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class ResourcesTag < ActiveRecord::Base
  belongs_to :resource
  belongs_to :tag
end
