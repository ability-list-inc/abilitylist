# == Schema Information
#
# Table name: saveds
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  resource_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Saved < ActiveRecord::Base
	belongs_to :user
	belongs_to :resource
end
