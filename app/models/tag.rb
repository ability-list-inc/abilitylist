# == Schema Information
#
# Table name: tags
#
#  id             :integer          not null, primary key
#  name           :string
#  taggings_count :integer          default(0)
#

class Tag < ActiveRecord::Base
	has_many :resources_tags
  has_many :resources, through: :resources_tags
end
