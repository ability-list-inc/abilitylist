# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string
#  first_name             :string
#  last_name              :string
#  dob                    :date
#  address_street         :string
#  address_city           :string
#  address_state          :string
#  address_postal         :string
#  phone                  :string
#  created_at             :datetime
#  updated_at             :datetime
#  encrypted_password     :string
#  password               :string
#  password_confirmation  :string
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  sign_in_count          :integer
#  remember_created_at    :datetime
#  permissions            :integer          default(0)
#  latitude               :float
#  longitude              :float
#  bio                    :text
#  views                  :integer          default(0), not null
#  name                   :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  resource_count         :integer          default(0)
#  interested_in          :text
#  can_share              :text
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  #devise :database_authenticatable, :registerable,
  #       :recoverable, :rememberable, :trackable, :validatable

	acts_as_taggable_on :epithets

	TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable
  devise :database_authenticatable, :registerable, :confirmable, :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  validates_format_of :email, :without => TEMP_EMAIL_REGEX, on: :update

	geocoded_by :full_street_address   # can also be an IP address
	after_validation :geocode          # auto-fetch coordinates

  has_many :resources
	has_many :conversations, through: :conversations_users
	has_many :friends, through: :friendships
	has_many :conversations_users
	has_many :saveds, :dependent => :destroy
	has_many :ratings, :dependent => :destroy
	has_many :request_access, :dependent => :destroy
	has_many :flags, :dependent => :destroy
	has_one :user_upload, :dependent => :destroy

	is_impressionable :counter_cache => true, :column_name => :views

	def self.format_this_num(num)
		if num < 1000
			return num
		elsif num < 1000000
			return (((num.to_f / 1000) * 10).ceil/10.0).to_s + "k"
		else
			return num
		end
	end

  def self.find_for_oauth(auth, signed_in_resource = nil)

    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(auth)

    # If a signed_in_resource is provided it always overrides the existing user
    # to prevent the identity being locked with accidentally created accounts.
    # Note that this may leave zombie accounts (with no associated identity) which
    # can be cleaned up at a later date.
    user = signed_in_resource ? signed_in_resource : identity.user

    # Create the user if needed
    if user.nil?

      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email if email_is_verified
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(
          name: auth.extra.raw_info.name,
          first_name: auth.extra.raw_info.first_name,
          last_name: auth.extra.raw_info.last_name,
          #username: auth.info.nickname || auth.uid,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        user.skip_confirmation!
        user.save!

				if auth.info.image.present?
					begin
				  	avatar_url = process_uri(auth.info.image + "?type=large")
				  	UserUpload.create({
				  		:photo => URI.parse(avatar_url),
				  		:user_id => user.id
						})
					rescue
						# do nothing
					end
				end
      end
    end

    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end

	def can_edit_resource(user_id)
		if self.id == user_id
			return true
		elsif self.permissions > 0
			return true
		else
			return false
		end
	end
	def can_favorite_resource(user_id)
		if self.id == user_id
			return true
		elsif self.permissions > 0
			return true
		else
			return false
		end
	end
	def can_edit_user_with_id(id)
		if self.id == id
			return true
		elsif self.permissions > 0
			return true
		else
			return false
		end
	end
	def can_manage_tags_for_resource(resource)
		if self.id == resource.user_id
			return true
		elsif self.permissions > 0
			return true
		else
			return false
		end
	end
	def full_street_address
		(self.address_street || "") + " " + (self.address_city || "") + " " + (self.address_state || "") + " " + (self.address_postal || "11249")
	end
  def confirmation_required?
    false
  end
  def age
  	dob = self.dob
  	if dob == nil
  		return "-"
  	else
  		now = Time.now.utc.to_date
			now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  	end
  end
	def full_name
		return first_name + " " + last_name
	end
	def fetch_photo(size)
		if user_upload
			user_upload.photo.url(size)
		else
			"/img/users/avatar.jpg"
		end
	end
	def location_description
		l = ""
		if address_city
			l += address_city
		end
		if address_city and address_state
			l += ", " + address_state
			return l
		elsif address_state
			return address_state
		else
			return l
		end
	end

  def profile_completeness
    completeness = 0
    unless self.first_name == nil or self.first_name.length < 2
      completeness += 10
    end

    unless self.last_name == nil or self.last_name.length < 2
      completeness += 10
    end

    unless self.address_city == nil or self.address_city.length < 2
      completeness += 10
    end

    unless self.address_state == nil or self.address_state.length < 2
      completeness += 10
    end

    unless self.address_postal == nil or self.address_postal.length < 5
      completeness += 20
    end

    unless self.bio == nil or self.bio.length < 5
      completeness += 20
    end

    unless UserUpload.where(user_id: self.id).length < 1
      completeness += 20
    end

    return completeness
  end

  def update_resource_count(count)

    self.resource_count = count
    self.save

  end

  def tasks_to_complete_profile
    tasks = []

    if self.first_name == nil or self.first_name.length < 2
      tasks.push("<a href='/users/#{self.id}/edit'>+ Set your first name (10%)</a>")
    end

    if self.last_name == nil or self.last_name.length < 2
      tasks.push("<a href='/users/#{self.id}/edit'>+ Set your last name (10%)</a>")
    end

    if self.address_city == nil or self.address_city.length < 2
      tasks.push("<a href='/users/#{self.id}/edit'>+ Set your city (10%)</a>")
    end

    if self.address_state == nil or self.address_state.length < 2
      tasks.push("<a href='/users/#{self.id}/edit'>+ Set your state (10%)</a>")
    end

    if self.address_postal == nil or self.address_postal.length < 5
      tasks.push("<a href='/users/#{self.id}/edit'>+ Set your postal code (20%)</a>")
    end

    if self.bio == nil or self.bio.length < 5
      tasks.push("<a data-toggle='modal' data-target='#myModal' style='cursor: pointer;' >+ Write a short bio (20%)</a>")
    end

    if UserUpload.where(user_id: self.id).length < 1
      tasks.push("<a href='/user_uploads/new'>+ Upload a photo (20%)</a>")
    end

    return tasks
  end
	def epithet_list
	  self.epithets.map(&:name).join(', ')
	end

	private

	def self.process_uri(uri)
    require 'open-uri'
    require 'open_uri_redirections'
    open(uri, :allow_redirections => :safe) do |r|
      r.base_uri.to_s
    end
  end
end
