# == Schema Information
#
# Table name: user_uploads
#
#  id                 :integer          not null, primary key
#  user_id            :integer
#  created_at         :datetime
#  updated_at         :datetime
#  photo_file_name    :string
#  photo_content_type :string
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#

class UserUpload < ActiveRecord::Base

	has_attached_file :photo, {:styles => { :medium => "220x220#", :thumb => "60x60#", :tiny => "40x40#" }}.merge(Abilitylist::Application.config.paperclip_defaults)

	crop_attached_file :photo
	
	validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/
	validates_attachment_presence :photo
	
	belongs_to :user

end
