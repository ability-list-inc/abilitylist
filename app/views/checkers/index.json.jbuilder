json.array!(@checkers) do |checker|
  json.extract! checker, :id, :full_address, :latitude, :longitude
  json.url checker_url(checker, format: :json)
end
