json.array!(@conversations) do |conversation|
  json.extract! conversation, :id, :last_message_sent_at, :access_token
  json.url conversation_url(conversation, format: :json)
end
