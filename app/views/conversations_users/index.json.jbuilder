json.array!(@conversations_users) do |conversations_user|
  json.extract! conversations_user, :id, :user_id, :conversation_id, :joined_at
  json.url conversations_user_url(conversations_user, format: :json)
end
