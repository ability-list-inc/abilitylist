json.array!(@distances) do |distance|
  json.extract! distance, :id, :location, :resource_id, :distance
  json.url distance_url(distance, format: :json)
end
