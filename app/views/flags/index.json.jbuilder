json.array!(@flags) do |flag|
  json.extract! flag, :id, :type, :resource_id, :user_id, :duplicate_id, :details
  json.url flag_url(flag, format: :json)
end
