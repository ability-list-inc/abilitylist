json.array!(@geocoded_addresses) do |geocoded_address|
  json.extract! geocoded_address, :id, :location, :latitude, :longitude, :address_street, :address_city, :address_state, :address_postal, :address_country
  json.url geocoded_address_url(geocoded_address, format: :json)
end
