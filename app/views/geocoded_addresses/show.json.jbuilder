json.extract! @geocoded_address, :id, :location, :latitude, :longitude, :address_street, :address_city, :address_state, :address_postal, :address_country, :created_at, :updated_at
