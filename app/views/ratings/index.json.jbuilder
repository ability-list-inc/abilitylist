json.array!(@ratings) do |rating|
  json.extract! rating, :id, :user_id, :resource_id, :score, :comment
  json.url rating_url(rating, format: :json)
end
