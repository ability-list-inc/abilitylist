json.array!(@request_accesses) do |request_access|
  json.extract! request_access, :id, :user_id, :resource_id, :explanation
  json.url request_access_url(request_access, format: :json)
end
