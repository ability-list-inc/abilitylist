json.array!(@resource_uploads) do |resource_upload|
  json.extract! resource_upload, :id, :resource_id
  json.url resource_upload_url(resource_upload, format: :json)
end
