json.array!(@resources) do |resource|
  json.extract! resource, :id, :name, :user_id, :address_street, :address_city, :address_state, :address_postal, :latitude, :longitude, :website, :description, :nationwide, :status, :phone, :views
  json.url resource_url(resource, format: :json)
end
