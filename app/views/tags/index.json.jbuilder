json.array!(@tags) do |tag|
  json.extract! tag, :id, :name, :count, :life_situation, :nonprofit_services, :one_time, :jobs, :housing, :forprofit_services, :marketplace
  json.url tag_url(tag, format: :json)
end
