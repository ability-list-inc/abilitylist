json.array!(@user_uploads) do |user_upload|
  json.extract! user_upload, :id, :user_id
  json.url user_upload_url(user_upload, format: :json)
end
