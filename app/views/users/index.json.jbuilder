json.array!(@users) do |user|
  json.extract! user, :id, :email, :password_hash, :password_salt, :first_name, :last_name, :dob, :gender, :super, :address_street, :address_city, :address_state, :address_postal, :phone
  json.url user_url(user, format: :json)
end
