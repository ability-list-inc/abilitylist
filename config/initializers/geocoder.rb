if !Rails.env.development? # production or staging 
	Geocoder.configure(
		:use_https => true,
	  :timeout => 15,
	  :google => {
	    :api_key => ENV['google_geocoding_api_key']
	  }
	)
else # development
	Geocoder.configure(
	  :timeout => 15
	)
end
