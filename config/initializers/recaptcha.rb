Recaptcha.configure do |config|
	# set to production keys or default to localhost:3000 keys to avoid errors
  config.public_key  = (ENV['recaptcha_public_key'] || "6Ld5PgoTAAAAAIglKSI94pRFYxR6D-Mur5L5zuzQ")
  config.private_key = (ENV['recaptcha_private_key'] || "6Ld5PgoTAAAAAFrqzZIpj8VJOTcRu2pngZS0R0tj")
  config.api_version = 'v2'
end