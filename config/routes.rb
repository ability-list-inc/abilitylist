Rails.application.routes.draw do

  resources :conversations_users
  resources :conversations
  resources :geocoded_addresses
  resources :distances
  resources :request_accesses
  resources :flags
  resources :ratings

  mount Bootsy::Engine => '/bootsy', as: 'bootsy'

  get 'search/new_search'
  get 'search/index'
  get 'search/classic'

  match "/classic_driver", to: 'search#classic_driver', via: 'get'
  match "/people_data", to: 'search#people_data', via: 'get'
  match "/accessibility", to: 'static#accessibility', via: 'get'

  match "/newConversation/:other_participant_id", to: 'conversations#create', via: 'get'

  match '/people', to: 'search#people', via: 'get'
  match '/search', to: 'search#classic', via: 'get'
  match '/browse', to: 'search#new_search', via: 'get'

  resources :saveds
  resources :user_uploads
  resources :resource_uploads
  resources :resources_tags
  resources :newsletters

  match 'submit_resource', to: 'static#submit_resource', via: 'post'
  match '/resources/new', to: 'resources#new', via: 'get'
  match '/resources/clarifyContent', to: 'resources#clarifyContent', via: 'get'
  match '/resources/:id', to: 'resources#rerouter', via: 'get'

  resources :resources do
    get :autocomplete_tag_name, :on => :collection
  end


  match '/r/:id/:resource_name', to: 'resources#show', via: 'get'
  resources :resources
  match '/resources/:id/categories', to: 'resources#categories', via: 'get'
  match '/resources/:id/weblinks', to: 'resources#weblinks', via: 'get'
  match '/resources/:id/photo', to: 'resources#photo', via: 'get'
  match '/resources/:id/success', to: 'resources#success', via: 'get'
  match '/resource_uploads/:id/crop', to: 'resource_uploads#crop', via: 'get'
  match '/user_uploads/:id/crop', to: 'user_uploads#crop', via: 'get'
  # this route is just like 'update' resources but the action reroutes to photo section
  match '/resources/:id/weblinks', to: 'resources#update_weblinks', via: 'post'

  # create a join table entry for resource-tag
  match '/resources_tags/:resource_id/:tag_id/create', to: 'resources_tags#custom_create', via: 'post'
  # destroy a join table entry for resource-tag
  match '/resources_tags/:resource_id/:tag_id/destroy', to: 'resources_tags#custom_destroy', via: 'post'

  # create a join table entry for saving 'favoriting' resources
  match '/saved/:resource_id/:user_id/create', to: 'saveds#custom_create', via: 'post'
  # destroy a join table entry for resource-tag
  match '/saved/:resource_id/:user_id/destroy', to: 'saveds#custom_destroy', via: 'post'

  match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup

  match '/users/:id/conversations' => 'users#conversations', via: 'get'
  match '/users/:id/conversations/:conversation_id' => 'users#conversation', via: 'get'

  match '/passwords/reset_sent' => 'static#reset_sent', via: 'get'

  devise_for :users, :controllers => { :passwords => "passwords", :sessions => 'users/sessions', :registrations => 'users/registrations', omniauth_callbacks: 'omniauth_callbacks' }
  resources :users do
    get :autocomplete_epithet_name, :on => :collection
  end

  root 'static#landing'

  get "/donate" => 'static#donate'
  get "/sign-up-raffle" => 'static#signupraffle'
  get "/about" => 'static#about'
  get "/igg" => 'static#igg'
  get "/thankyou" => 'static#thankyou'
  get "api" => 'static#api'

  # this is only used in testing
  devise_scope :user do
    match '/users/sign_up' => 'users/registrations#create', via: 'post'
  end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
