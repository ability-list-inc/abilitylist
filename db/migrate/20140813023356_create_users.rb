class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_hash
      t.string :password_salt
      t.string :first_name
      t.string :last_name
      t.date :dob
      t.string :gender
      t.integer :super
      t.string :address_street
      t.string :address_city
      t.string :address_state
      t.string :address_postal
      t.string :phone

      t.timestamps
    end
  end
end
