class AddEncryptedPasswordToUsers < ActiveRecord::Migration
  def change
    add_column :users, :encrypted_password, :string
    add_column :users, :password, :string
    add_column :users, :password_confirmation, :string

    remove_column :users, :password_hash
    remove_column :users, :password_salt

  end
end
