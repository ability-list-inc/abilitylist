class DropNonPcThingsFromUsers < ActiveRecord::Migration
  def change
    add_column :users, :permissions, :integer, default: 0

    remove_column :users, :super
    remove_column :users, :gender
  end
end
