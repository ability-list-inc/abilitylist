class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|
      t.string :name
      t.integer :user_id
      t.string :address_street
      t.string :address_city
      t.string :address_state
      t.string :address_postal
      t.float :geo_lat
      t.float :geo_lng
      t.string :website
      t.text :description
      t.integer :nationwide, :default => 0
      t.integer :status, :default => 0
      t.string :phone
      t.integer :views

      t.timestamps
    end
  end
end
