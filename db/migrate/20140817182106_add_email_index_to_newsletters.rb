class AddEmailIndexToNewsletters < ActiveRecord::Migration
  def change
		add_index :newsletters, :email, :unique => true
  end
end
