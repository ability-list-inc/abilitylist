class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name
      t.integer :count
      t.boolean :life_situation
      t.boolean :nonprofit_services
      t.boolean :one_time
      t.boolean :jobs
      t.boolean :housing
      t.boolean :forprofit_services
      t.boolean :marketplace

      t.timestamps
    end
  end
end
