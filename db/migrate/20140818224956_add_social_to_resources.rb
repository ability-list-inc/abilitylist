class AddSocialToResources < ActiveRecord::Migration
  def change
    add_column :resources, :basics_complete, :boolean, :default => false
    add_column :resources, :categories_complete, :boolean, :default => false
    add_column :resources, :weblinks_complete, :boolean, :default => false
    add_column :resources, :photo_complete, :boolean, :default => false
    add_column :resources, :facebook, :string
    add_column :resources, :twitter, :string
    add_column :resources, :linkedin, :string
    add_column :resources, :yelp, :string
    add_column :resources, :googleplus, :string
    add_column :resources, :foursquare, :string
  end
end
