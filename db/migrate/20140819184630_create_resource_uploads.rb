class CreateResourceUploads < ActiveRecord::Migration
  def change
    create_table :resource_uploads do |t|
      t.integer :resource_id

      t.timestamps
    end
  end
end
