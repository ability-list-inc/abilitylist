class AddAttachmentPhotoToResourceUploads < ActiveRecord::Migration
  def self.up

    change_table :resource_uploads do |t|
      t.attachment :photo
    end
  end

  def self.down
    remove_attachment :resource_uploads, :photo
  end
end
