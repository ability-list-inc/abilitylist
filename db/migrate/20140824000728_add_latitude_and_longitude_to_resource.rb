class AddLatitudeAndLongitudeToResource < ActiveRecord::Migration
  def change
    add_column :resources, :latitude, :float
    add_column :resources, :longitude, :float

    remove_column :resources, :geo_lat
    remove_column :resources, :geo_lng    
  end
end
