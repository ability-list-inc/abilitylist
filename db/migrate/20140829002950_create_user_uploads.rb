class CreateUserUploads < ActiveRecord::Migration
  def change
    create_table :user_uploads do |t|
      t.integer :user_id

      t.timestamps
    end
  end
end
