class MakeResourceViewsDefaultZero < ActiveRecord::Migration
  def change
  	remove_column :resources, :views
  	add_column :resources, :views, :integer, :null => false, :default => 0
  	add_column :users, :views, :integer, :null => false, :default => 0
  end
end
