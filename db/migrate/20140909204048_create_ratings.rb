class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :user_id
      t.integer :resource_id
      t.integer :score
      t.text :comment

      t.timestamps
    end
  end
end
