class ChangeRatingScoreFromIntegerToFloat < ActiveRecord::Migration
  def change
  	remove_column :ratings, :score
  	add_column :ratings, :score, :float
  end
end
