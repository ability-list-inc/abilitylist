class AddCurrentAverageRatingToResource < ActiveRecord::Migration
  def change
    add_column :resources, :current_average_rating, :float, :null => false, :default => -1
  end
end
