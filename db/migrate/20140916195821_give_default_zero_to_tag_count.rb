class GiveDefaultZeroToTagCount < ActiveRecord::Migration
  def change
		remove_column :tags, :count
		add_column :tags, :count, :integer, :null => false, :default => 0
  end
end
