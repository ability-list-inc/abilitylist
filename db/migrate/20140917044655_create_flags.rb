class CreateFlags < ActiveRecord::Migration
  def change
    create_table :flags do |t|
      t.string :type
      t.integer :resource_id
      t.integer :user_id
      t.integer :duplicate_id
      t.text :details

      t.timestamps
    end
  end
end
