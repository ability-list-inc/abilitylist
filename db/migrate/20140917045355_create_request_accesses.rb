class CreateRequestAccesses < ActiveRecord::Migration
  def change
    create_table :request_accesses do |t|
      t.integer :user_id
      t.integer :resource_id
      t.text :explanation

      t.timestamps
    end
  end
end
