class AddClosedToRequestAccess < ActiveRecord::Migration
  def change
    add_column :request_accesses, :closed, :integer, :null => false, :default => 0
  end
end
