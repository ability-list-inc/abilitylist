class AddCategoryToFlag < ActiveRecord::Migration
  def change
		remove_column :flags, :type
    add_column :flags, :category, :string
  end
end
