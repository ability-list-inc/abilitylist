class AddFieldsToResource < ActiveRecord::Migration
  def change
    add_column :resources, :validated_city, :string
    add_column :resources, :validated_postal, :string
    add_column :resources, :validated_country, :string
  end
end
