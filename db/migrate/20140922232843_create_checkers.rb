class CreateCheckers < ActiveRecord::Migration
  def change
    create_table :checkers do |t|
      t.string :full_address
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
