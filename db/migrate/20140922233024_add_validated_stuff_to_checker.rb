class AddValidatedStuffToChecker < ActiveRecord::Migration
  def change
    add_column :checkers, :validated_city, :string
    add_column :checkers, :validated_postal, :string
    add_column :checkers, :validated_country, :string
  end
end
