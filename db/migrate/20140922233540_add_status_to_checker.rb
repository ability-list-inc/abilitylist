class AddStatusToChecker < ActiveRecord::Migration
  def change
    add_column :checkers, :status, :integer
  end
end
