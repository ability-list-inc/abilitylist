class AddIndexToResourcesOnUpdatedAt < ActiveRecord::Migration
  def change
		add_index :resources, :updated_at
  end
end
