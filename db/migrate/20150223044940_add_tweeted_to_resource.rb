class AddTweetedToResource < ActiveRecord::Migration
  def change
    add_column :resources, :tweeted, :boolean
  end
end
