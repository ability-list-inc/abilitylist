class AddDefaultToTweeted < ActiveRecord::Migration
  def change
		remove_column :resources, :tweeted
    add_column :resources, :tweeted, :boolean, default: false
  end
end
