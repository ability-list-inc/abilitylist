class AddAnonymousToResource < ActiveRecord::Migration
  def change
    add_column :resources, :anonymous, :boolean, :default => false
  end
end
