class AddResourceCountToUser < ActiveRecord::Migration
  def change
    add_column :users, :resource_count, :integer, :default => 0
  end
end
