class AddTagListStringToResources < ActiveRecord::Migration
  def change
    add_column :resources, :tag_list_string, :text
  end
end
