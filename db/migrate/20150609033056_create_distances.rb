class CreateDistances < ActiveRecord::Migration
  def change
    create_table :distances do |t|
      t.string :location
      t.integer :resource_id
      t.float :distance

      t.timestamps null: false
    end
  end
end
