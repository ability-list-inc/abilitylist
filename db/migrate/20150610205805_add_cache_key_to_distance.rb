class AddCacheKeyToDistance < ActiveRecord::Migration
  def change
    add_column :distances, :cache_key, :string
		add_index :distances, :cache_key, :unique => true
  end
end
