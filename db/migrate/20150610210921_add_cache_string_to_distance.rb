class AddCacheStringToDistance < ActiveRecord::Migration
  def change
		remove_index :distances, :cache_key
    remove_column :distances, :cache_key, :string

    add_column :distances, :cache_string, :string
		add_index :distances, :cache_string, :unique => true
  end
end
