class CreateGeocodedAddresses < ActiveRecord::Migration
  def change
    create_table :geocoded_addresses do |t|
      t.string :location
      t.float :latitude
      t.float :longitude
      t.string :address_street
      t.string :address_city
      t.string :address_state
      t.string :address_postal
      t.string :address_country

      t.timestamps null: false
    end
  end
end
