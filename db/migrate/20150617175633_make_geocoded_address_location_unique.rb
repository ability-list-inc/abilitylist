class MakeGeocodedAddressLocationUnique < ActiveRecord::Migration
  def change
		add_index :geocoded_addresses, :location, :unique => true
  end
end
