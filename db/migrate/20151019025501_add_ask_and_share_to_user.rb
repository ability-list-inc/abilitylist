class AddAskAndShareToUser < ActiveRecord::Migration
  def change
    add_column :users, :interested_in, :text
    add_column :users, :can_share, :text
  end
end
