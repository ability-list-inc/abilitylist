class CreateConversations < ActiveRecord::Migration
  def change
    create_table :conversations do |t|
      t.datetime :last_message_sent_at
      t.string :access_token

      t.timestamps null: false
    end
  end
end
