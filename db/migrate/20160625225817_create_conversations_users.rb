class CreateConversationsUsers < ActiveRecord::Migration
  def change
    create_table :conversations_users do |t|
      t.integer :user_id
      t.integer :conversation_id
      t.datetime :joined_at

      t.timestamps null: false
    end
  end
end
