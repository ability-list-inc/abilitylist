# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160710172628) do

  create_table "bootsy_image_galleries", force: :cascade do |t|
    t.integer  "bootsy_resource_id"
    t.string   "bootsy_resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bootsy_images", force: :cascade do |t|
    t.string   "image_file"
    t.integer  "image_gallery_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "checkers", force: :cascade do |t|
    t.string   "full_address"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "validated_city"
    t.string   "validated_postal"
    t.string   "validated_country"
    t.integer  "status"
  end

  create_table "conversations", force: :cascade do |t|
    t.datetime "last_message_sent_at"
    t.string   "access_token"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "conversations_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "conversation_id"
    t.datetime "joined_at"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "distances", force: :cascade do |t|
    t.string   "location"
    t.integer  "resource_id"
    t.float    "distance"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "cache_string"
  end

  add_index "distances", ["cache_string"], name: "index_distances_on_cache_string", unique: true

  create_table "flags", force: :cascade do |t|
    t.integer  "resource_id"
    t.integer  "user_id"
    t.integer  "duplicate_id"
    t.text     "details"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "category"
  end

  create_table "friendships", force: :cascade do |t|
    t.integer  "user_1_id"
    t.integer  "status",     default: 1
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "geocoded_addresses", force: :cascade do |t|
    t.string   "location"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "address_street"
    t.string   "address_city"
    t.string   "address_state"
    t.string   "address_postal"
    t.string   "address_country"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "geocoded_addresses", ["location"], name: "index_geocoded_addresses_on_location", unique: true

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id"

  create_table "impressions", force: :cascade do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "impressions", ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index"
  add_index "impressions", ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index"
  add_index "impressions", ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index"
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index"
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index"
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index"
  add_index "impressions", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index"
  add_index "impressions", ["user_id"], name: "index_impressions_on_user_id"

  create_table "newsletters", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "postal"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "newsletters", ["email"], name: "index_newsletters_on_email", unique: true

  create_table "ratings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "resource_id"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "score"
  end

  create_table "request_accesses", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "resource_id"
    t.text     "explanation"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "closed",      default: 0, null: false
  end

  create_table "resource_uploads", force: :cascade do |t|
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "resources", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.string   "address_street"
    t.string   "address_city"
    t.string   "address_state"
    t.string   "address_postal"
    t.string   "website"
    t.text     "description"
    t.integer  "nationwide",             default: 0
    t.integer  "status",                 default: 0
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "basics_complete",        default: false
    t.boolean  "categories_complete",    default: false
    t.boolean  "weblinks_complete",      default: false
    t.boolean  "photo_complete",         default: false
    t.string   "facebook"
    t.string   "twitter"
    t.string   "linkedin"
    t.string   "yelp"
    t.string   "googleplus"
    t.string   "foursquare"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "views",                  default: 0,     null: false
    t.float    "current_average_rating", default: -1.0,  null: false
    t.string   "email"
    t.string   "validated_city"
    t.string   "validated_postal"
    t.string   "validated_country"
    t.boolean  "tweeted",                default: false
    t.boolean  "anonymous",              default: false
    t.string   "route_to"
    t.text     "tag_list_string"
  end

  add_index "resources", ["updated_at"], name: "index_resources_on_updated_at"

  create_table "resources_tags", force: :cascade do |t|
    t.integer  "resource_id"
    t.integer  "tag_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "resources_tags", ["resource_id"], name: "index_resources_tags_on_resource_id"
  add_index "resources_tags", ["tag_id"], name: "index_resources_tags_on_tag_id"

  create_table "saveds", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true

  create_table "user_uploads", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.date     "dob"
    t.string   "address_street"
    t.string   "address_city"
    t.string   "address_state"
    t.string   "address_postal"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password"
    t.string   "password"
    t.string   "password_confirmation"
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "sign_in_count"
    t.datetime "remember_created_at"
    t.integer  "permissions",            default: 0
    t.float    "latitude"
    t.float    "longitude"
    t.text     "bio"
    t.integer  "views",                  default: 0, null: false
    t.string   "name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer  "resource_count",         default: 0
    t.text     "interested_in"
    t.text     "can_share"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true

end
