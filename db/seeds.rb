# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create( first_name: "Andrew", last_name: "Zitek", email: "andrewzitek@abilitylist.org", password: "test2121", password_confirmation: "test2121", address_street: "240 Kent Ave", address_city: "Brooklyn", address_state: "NY", address_postal: "11249", permissions: 1)
User.create( first_name: "Theresa", last_name: "Previ", email: "theresaprevi@abilitylist.org", password: "test2121", password_confirmation: "test2121", address_street: "240 Kent Ave", address_city: "Brooklyn", address_state: "NY", address_postal: "11249", permissions: 1)
User.create( first_name: "Ben", last_name: "Kim", email: "benkim@abilitylist.org", password: "test2121", password_confirmation: "test2121", address_street: "240 Kent Ave", address_city: "Brooklyn", address_state: "NY", address_postal: "11249", permissions: 1)
User.create( first_name: "Robert", last_name: "Gil", email: "robertgil@abilitylist.org", password: "test2121", password_confirmation: "test2121", address_street: "240 Kent Ave", address_city: "Brooklyn", address_state: "NY", address_postal: "11249", permissions: 1)
User.create( first_name: "Andrew", last_name: "Horn", email: "andrewhorn@abilitylist.org", password: "test2121", password_confirmation: "test2121", address_street: "240 Kent Ave", address_city: "Brooklyn", address_state: "NY", address_postal: "11249", permissions: 1)

#USER
for i in 0..9
  u = User.create(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, address_street: Faker::Address.street_name, address_city: Faker::Address.city, address_state: Faker::Address.state, address_postal: Faker::Address.postcode, password: "test2121", password_confirmation: "test2121", email: Faker::Internet.free_email)
  UserUpload.create({ 
    :photo => File.new("#{Rails.root}/public/seeds/avatars/photo_#{rand(1..41)}.png"),
    :user_id => u.id
  })
end

for i in 1..9

  if rand(0..1) == 1 
    r = Resource.create(user_id: i, name: Faker::Company.name, address_street: Faker::Address.street_name, address_city: Faker::Address.city, address_state: Faker::Address.state, address_postal: Faker::Address.postcode, website: Faker::Internet.url, email: Faker::Internet.free_email, phone: Faker::PhoneNumber.cell_phone, description: Faker::Lorem.paragraph(5), longitude: Faker::Address.longitude, latitude: Faker::Address.latitude, photo_complete: true, categories_complete: true, weblinks_complete: true, basics_complete: true  )
    r.clean_tags_string
    r.save
  end
  if rand(0..1) == 1 
    r = Resource.create(user_id: i, name: Faker::Company.name, address_street: Faker::Address.street_name, address_city: Faker::Address.city, address_state: Faker::Address.state, address_postal: Faker::Address.postcode, website: Faker::Internet.url, email: Faker::Internet.free_email, phone: Faker::PhoneNumber.cell_phone, description: Faker::Lorem.paragraph(5), longitude: Faker::Address.longitude, latitude: Faker::Address.latitude, photo_complete: true, categories_complete: true, weblinks_complete: true, basics_complete: true )
    r.clean_tags_string
    r.save
  end
  if rand(0..1) == 1 
    r = Resource.create(user_id: i, name: Faker::Company.name, address_street: Faker::Address.street_name, address_city: Faker::Address.city, address_state: Faker::Address.state, address_postal: Faker::Address.postcode, website: Faker::Internet.url, email: Faker::Internet.free_email, phone: Faker::PhoneNumber.cell_phone, description: Faker::Lorem.paragraph(5), longitude: Faker::Address.longitude, latitude: Faker::Address.latitude, photo_complete: true, nationwide: true, categories_complete: true, weblinks_complete: true, basics_complete: true )
    r.clean_tags_string
    r.save
  end

end

tags = ["housing", "education", "transportation", "jobs", "recreation", "medical resources", "assistance", "organizations", "events"]

resources = Resource.all
resources.each do |r|

  r.tap do |res|
    res.tag_list.add(tags[rand(0..(tags.length/2)-1)])
    res.tag_list.add(tags[rand((tags.length/2)..tags.length-1)])
    res.save
  end

  ResourceUpload.create({
    :photo => Faker::Company.logo,
    :resource_id => r.id
    })
end

