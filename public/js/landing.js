var SearchFlow = function() {
	this.postal = null;
	this.situation = null;

	SearchFlow.prototype.check_for_valid_postal = function (input) {
		var value = input.val();
		this.postal = value;
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(value)) {
			// valid 5 digit US postal code
			//console.log(true);
			this.add_category_input();
		} else {
			// not valid 5 digit US postal code
			console.log(false);
			this.remove_category_input();
		}
	}

	SearchFlow.prototype.add_category_input = function () {
		document.getElementById("category-canvas").style.display = "inline-block";
	}

	SearchFlow.prototype.remove_category_input = function () {
		document.getElementById("category-canvas").style.display = "none";			
	}

}

var homepage_flow = new SearchFlow();

$('input[name=location]').on('input',function(e){
	homepage_flow.check_for_valid_postal($('input[name=location]')); 
});

$('.selectpicker').selectpicker({
	dropupAuto: false
});