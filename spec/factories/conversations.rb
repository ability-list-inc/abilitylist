# == Schema Information
#
# Table name: conversations
#
#  id                   :integer          not null, primary key
#  last_message_sent_at :datetime
#  access_token         :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

FactoryGirl.define do
  factory :conversation do
    last_message_sent_at "2016-06-25 18:53:25"
access_token "MyString"
  end

end
