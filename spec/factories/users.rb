# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string
#  first_name             :string
#  last_name              :string
#  dob                    :date
#  address_street         :string
#  address_city           :string
#  address_state          :string
#  address_postal         :string
#  phone                  :string
#  created_at             :datetime
#  updated_at             :datetime
#  encrypted_password     :string
#  password               :string
#  password_confirmation  :string
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  sign_in_count          :integer
#  remember_created_at    :datetime
#  permissions            :integer          default(0)
#  latitude               :float
#  longitude              :float
#  bio                    :text
#  views                  :integer          default(0), not null
#  name                   :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  resource_count         :integer          default(0)
#  interested_in          :text
#  can_share              :text
#

FactoryGirl.define do
  factory :user do
    name "Test User"
    sequence(:email) { |n| "test#{n}@example.com" }
    password "please123"
  end
end
