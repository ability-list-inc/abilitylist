# Feature: Navigation links
#   As a visitor
#   I want to see navigation links
#   So I can find home, sign in, or sign up
feature 'Navigation links', :devise do

  # Scenario: View navigation links
  #   Given I am a visitor
  #   When I visit the home page
  #   Then I see "home," "sign in," and "sign up"
  scenario 'view navigation links' do
    visit root_path
    expect(page).to have_content 'How it Works'
    expect(page).to have_content 'Share'
    expect(page).to have_content 'Browse'
    expect(page).to have_content 'About'
    expect(page).to have_content 'Donate Now'
    expect(page).to have_content 'Login / Sign Up'
  end

end
